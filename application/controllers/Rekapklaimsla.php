<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapklaimsla extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Rekapklaimsla_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Rekap Klaim SLA';
        $data['sub_menu']   = 22;
        $data['page_id']    = 13;

        $data['datana']         = $this->Rekapklaimsla_m->dokumen();

            $this->template->load('template','rekapklaimsla',$data);
    }

}