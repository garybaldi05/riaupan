<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SimulasiKalkulator extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Dokumenriau_m');
        $this->load->model('SimulasiKalkulator_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Simulasi Biaya Nasabah';
        $data['sub_menu']   = 21;
        $data['page_id']    = 6;

        $data['datana']         = $this->Dokumenriau_m->dokumen();

        $this->template->load('template','simulasikalkulator',$data);
    }

    function simulasi()
    {

        extract($_POST);

        $data['title']      = 'Simulasi Biaya Nasabah';
        $data['sub_menu']   = 21;
        $data['page_id']    = 6;

        $lahir = date("Ymd", strtotime($tgllahir));
        $tanggal = new DateTime($lahir);
        $today = new DateTime('today');
        $y = $today->diff($tanggal)->y;
        $m = $today->diff($tanggal)->m;
        $d = $today->diff($tanggal)->d;
        if ($m >= 6 && $d > 0) {
            $year = $y + 1;
        } else{
            $year = $y;
        }

        if (!empty($jangkawaktu)) {
            $yearss = $jangkawaktu/12;
            $pembulatan = number_format((float)$yearss, 1);
            $yea = substr(strrchr($pembulatan, "."), 1);
                if ($yea > 6) {
                    $jangkawaktu = floor($pembulatan) + 1;
                } else{
                    $jangkawaktu = floor($pembulatan);
                }
        } else{
            $jangkawaktu = '0';
        }

        $akad = date("Ymd", strtotime($tglakad));

        $amount = preg_replace('/\D/', '', $plafon);

        // var_dump($_POST);
        // die();

        $medcheck = $this->db->query("select a.id_pekerjaan, c.id_type, a.kode_jenisdeb, b.status FROM PAN_BRK.dbo.MasterDebitur a
            left join PAN_BRK.dbo.MedicalCheckup b on b.kode_jenisdeb = a.kode_jenisdeb
            left join PAN_BRK.dbo.TypeManfaat c on c.kode_jenisdeb = a.kode_jenisdeb
            where a.id_pekerjaan = '$pekerjaan' and
            '$amount' between b.plafon_min and b.plafon_max
            and '$year' between b.usia_min and b.usia_max
            group by a.kode_jenisdeb, b.status, a.id_pekerjaan, c.id_type")->result_array();

            if ($plankredit == '326' || $plankredit == '358') {
                $kode = 'INTERNALBANK';
            } else{
                if (!empty($medcheck[0]['kode_jenisdeb'])) {
                    $kode = $medcheck[0]['kode_jenisdeb'];
                } else{
                    $kode = '';
                }
            }

            if ($kode == 'PENSIUNAN' || $year >= 55) {
                $idtypes = '13';
            } elseif ($kode == 'UMUM01') {
                $idtypes = '1';
            } elseif ($kode == 'KHUSUS01') {
                $idtypes = '2';
            } elseif ($kode == 'INTERNALBANK') {
                $idtypes = '6';
                $id = '43';
            } elseif ($kode == 'DEWAN') {
                $idtypes = '4';
            } else{
                if (!empty($medcheck[0]['id_type'])) {
                    $idtypes = $medcheck[0]['id_type'];
                } else{
                    $idtypes = '';
                }
            }

            $type = $this->db->query("select * from PAN_BRK.dbo.Rate a
            join PAN_BRK.dbo.TypeManfaat b on a.id_type = b.id_type
            join PAN_BRK.dbo.MasterDebitur c on c.kode_jenisdeb = b.kode_jenisdeb
            join PAN_BRK.dbo.MedicalCheckup d on d.kode_jenisdeb = c.kode_jenisdeb
            where c.kode_jenisdeb = '$kode' and a.jangka_waktu = '$jangkawaktu' and a.id_type = '$idtypes' and c.id_pekerjaan = '$pekerjaan' and '$amount' between d.plafon_min and d.plafon_max and '$year' between d.usia_min and d.usia_max")->result_array();

            if (empty($type) || $type == '' || $type == null) {
                $rate = '0'; 
            } else{
                $rate = $type[0]['rate'];
            }

            if ($amount != '' && $rate != '') {
                $a = ($amount * $rate) / 1000;
            }

            if (!empty($a)) {
                $premi = number_format(floatval($a), 0);
            } else{
                $premi = '0';
            }

            $data['datas'] = array('premi' => $premi, 'plafon' => $plafon, 'rate' => $rate);
            // $data['datas'] = $data;

            // var_dump($data);
            // die();

        
        // $data['datas']         = $premi;
        // $data['datas']         = $plafon;
        // $data['datas']         = $rate;

        $this->template->load('template','simulasikalkulator',$data);
    }

    function get_tipe()
    {
        $id=$this->input->post('id');
        $data=$this->SimulasiKalkulator_m->get_tipe($id);
        echo json_encode($data);
    }

}