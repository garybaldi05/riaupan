<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historirekon extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Historirekon_m');
        $this->load->helper(array('url','download')); 

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        extract($_POST);

        // var_dump($_POST['id_data']);
        // die();

        $id_data = $_POST['id_data'];

        $data['title']      = 'Rekonsiliasi';
        $data['sub_menu']   = 21;
        $data['page_id']    = 30;

        $data['datana']         = $this->Historirekon_m->dokumen($id_data);
        $data['datarekon']         = $this->Historirekon_m->datarekon($id_data);

        $this->template->load('template','historirekon',$data);
    }

    function submitrekon()
    {
        extract($_POST);

        // var_dump($_POST);
        // die();
        

        $tglpembayaran = date("Ymd", strtotime($tglpembayaran));
        if ($jenis_pembayaran == 'pembayaranbank') {
            $jenis = 'Pembayaran Bank';
            $jumlahbayar = $jumlahpembayaran;
            $sqll = $this->db->query("select sum(jumlahpembayaran) as amounts from PAN_BRK.dbo.Histori_Rekon where id_data = '$id_data'")->result_array();
        } elseif ($jenis_pembayaran == 'penarikanbank') {
            $jenis = 'Penarikan Bank';
            $jumlahbayar = '-'.$jumlahpembayaran;
            $update = str_replace(',', '', $jumlah_bayar) - $jumlahpembayaran;
        }

        $this->db->query("insert into PAN_BRK.dbo.Histori_Rekon 
                (id_data, nama, norek, pk, jenis_pembayaran, amount, rate_asuransi, jumlahpembayaran, tglpembayaran, createdby, createddate) 
                values ('$id_data', '$nama', '$norek', '$pk', '$jenis', '$amount', '$rate_asuransi', '$jumlahbayar', '$tglpembayaran', '".$this->session->userdata('NamaUser')."', GETDATE())");

            $sqll = $this->db->query("select sum(jumlahpembayaran) as amounts from PAN_BRK.dbo.Histori_Rekon where id_data = '$id_data'")->result_array();

            if (!empty($sqll)) {
                  foreach ($sqll as $key => $values) {
                      $saldo = number_format($values['amounts']);
                  }
            } else{
                $saldo = '0';
            }

            $this->db->query("update PAN_BRK.dbo.DataRiau set jumlah_bayar = '$saldo' where id_data = '$id_data'");

            $this->session->set_flashdata('success', 'Update Rekonsiliasi Success');
            redirect('rekonsiliasi');
    }

    function delete($id_data)
    {
            $this->db->query("update PAN_BRK.dbo.DataRiau set status = '0'
            where id_data = '$id_data'");
            $this->session->set_flashdata('success', 'Delete Data Successfully');
            redirect('dokumenriau');
    }

}