<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftarpolisasuransi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Daftarpolisasuransi_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Daftar Polis Asuransi';
        $data['sub_menu']   = 21;
        $data['page_id']    = 9;

        $data['datana']         = $this->Daftarpolisasuransi_m->dokumen();

            $this->template->load('template','daftarpolisasuransi',$data);
    }

    function get_capem()
    {
        $id=$this->input->post('id');
        $data=$this->Daftarpolisasuransi_m->get_capem($id);
        echo json_encode($data);
    }

    function search()
    {

        extract($_POST);

        $data['title']      = 'Daftar Polis Asuransi';
        $data['sub_menu']   = 21;
        $data['page_id']    = 7;

        $data['search']         = $this->Daftarpolisasuransi_m->search();

        if (!empty($cabang) || !empty($capem) || !empty($periodebulan) || !empty($periodetahun)){
            $data['datana']         = $this->Daftarpolisasuransi_m->search();
        } else{
            $data['datana']         = $this->Daftarpolisasuransi_m->dokumen();
        }
        $this->template->load('template','daftarpolisasuransi',$data);
    }

}