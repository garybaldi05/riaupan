<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Casebycase extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Casebycase_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Case by Case';
        $data['sub_menu']   = 21;
        $data['page_id']    = 20;

        $data['datana']         = $this->Casebycase_m->dokumen();

            $this->template->load('template','casebycase',$data);
    }

    function get_capem()
    {
        $id=$this->input->post('id');
        $data=$this->Casebycase_m->get_capem($id);
        echo json_encode($data);
    }

    function search()
    {

        extract($_POST);

        $data['title']      = 'Case by Case';
        $data['sub_menu']   = 21;
        $data['page_id']    = 20;

        $data['search']         = $this->Casebycase_m->search();

        if (!empty($cabang) || !empty($capem) || !empty($periodebulan) || !empty($periodetahun)){
            $data['datana']         = $this->Casebycase_m->search();
        } else{
            $data['datana']         = $this->Casebycase_m->dokumen();
        }
        $this->template->load('template','casebycase',$data);
    }

    function SendDataTampungan($id){
        $sql = $this->db->query("select * from PAN_BRK.dbo.DataRiau where id_data = '$id'")->result_array();

        if (!empty($sql)) {
            foreach ($sql as $key => $value) {
               
                // $arr_insert = array(
                //     'id_transaksi' => $id_transaksi,
                //     'id_transaksi_bank' => $id_transaksi_bank,
                //     'id_pengajuan' => $value['id_pengajuan'],
                //     'kode_cabang' => $value['kode_cabang'],
                //     'kode_broker' => $value['kode_broker'],
                //     'nama' => $value['nama'],
                //     'ktp' => $value['ktp'],
                //     'status_dokumen' => $value['status_dokcbc'],
                //     'premi_disetujui' => $value['premi_disetujui'],
                //     'keterangan' => '-'
                //     );


                // =========> Try Dummy Data

                $arr_insert = array(
                'id_transaksi' => '009',
                'id_transaksi_bank' => '001',
                'id_pengajuan' => 'PCBC-001',
                'kode_cabang' => '103',
                'kode_broker' => '003',
                'nama' => 'Hana Setiawan',
                'ktp' => '147100812510640082',
                'status_dokumen' => '2',
                'premi_disetujui' => 12000000,
                'keterangan' => '-'
                );

                $ch = curl_init();
                $data_string = json_encode($arr_insert);
                // curl_setopt($ch, CURLOPT_HEADER, false);
                // curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
                // curl_setopt($ch, CURLOPT_URL, "https://login.nusabroker.com:888/riaupan/Callbackapi");
                curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1/api/CallBackRiau/UpdateDataCBC"); // Isi dengan URL apps tampungan
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                // curl_setopt($ch, CURLOPT_PORT, 888);
                // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,1);
                // curl_setopt($ch, CURLOPT_TIMEOUT, 120);
                // curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 600);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                                            'Content-Type: application/json'
                                                        ));
                // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($ch);
                curl_close($ch);

                // if(curl_errno($ch)){
                //     $err = curl_error($ch);
                //     echo $err;
                // }
                $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    // echo $httpcode;
                        if($httpcode == '200'){
                            $json = json_decode($result, true);

                        if($json['Result']['kode_response'] == '00'){
                            // $return = array("Result"=>array('status' => '200', 'kode_response' => '00', "message"=>"Berhasil Update Data ke Bank"));
                            return $json['Result']['message'];
                        }else{
                            // $return = array("Result"=>array('status' => '200', 'kode_response' => '99', "message"=>"Gagal Update Data ke Bank"));
                            return $json['Result']['message'];
                        }
                    }
                }
        }

    }


    function update($id_data)
    {
        extract($_POST);

        var_dump($_POST);
        die();

        $updateTampungan = $this->SendDataTampungan($id_data);

        if(strpos($updateTampungan, 'Gagal') !== false){ 
        $this->db->query("update PAN_BRK.dbo.DataRiau set status_dokcbc = '$status', keterangan = '$keterangan',
            modifiedby = '".$this->session->userdata('NamaUser')."', date_modified = GETDATE() where id_data = '$id_data'");
        $this->session->set_flashdata('success', 'Update Dokumen Tidak Lengkap Success');
        }else{
            $this->session->set_flashdata('success', 'Gagal Update Dokumen Message : '. $updateTampungan);
        }
        
    }

}