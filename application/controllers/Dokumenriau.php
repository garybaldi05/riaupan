<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumenriau extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Dokumenriau_m');
        $this->load->helper(array('url','download')); 

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Penutupan Asuransi';
        $data['sub_menu']   = 21;
        $data['page_id']    = 6;

        // $data['datana']         = $this->Dokumenriau_m->dokumen();

        if (empty($this->uri->segment(3))) {
          $perpage = 0;
        } else{
        $perpage = $this->uri->segment(3);
        }
          $offset = 10;
         $data['datana'] = $this->Dokumenriau_m->getDataPagination($perpage, $offset);

         $config['base_url'] = site_url('dokumenriau/index');
         $config['total_rows'] = $this->Dokumenriau_m->dokumen()->num_rows();
         $config['per_page'] = $offset;

         $config['next_link'] = 'Next';
          $config['prev_link'] = 'Previous';
          $config['first_link'] = 'First';
          $config['last_link'] = 'Last';
          $config['full_tag_open'] = '<ul class="pagination">';
          $config['full_tag_close'] = '</ul>';
          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
          $config['cur_tag_open'] = '<li class="active"><a href="#">';
          $config['cur_tag_close'] = '</a></li>';
          $config['prev_tag_open'] = '<li>';
          $config['prev_tag_close'] = '</li>';
          $config['next_tag_open'] = '<li>';
          $config['next_tag_close'] = '</li>';
          $config['last_tag_open'] = '<li>';
          $config['last_tag_close'] = '</li>';
          $config['first_tag_open'] = '<li>';
          $config['first_tag_close'] = '</li>';

         $this->pagination->initialize($config);

        $this->template->load('template','dokumenriau',$data);
    }

    function get_capem()
    {
        $id=$this->input->post('id');
        $data=$this->Dokumenriau_m->get_capem($id);
        echo json_encode($data);
    }

    function search()
    {

        extract($_POST);

        $data['title']      = 'Penutupan Asuransi';
        $data['sub_menu']   = 21;
        $data['page_id']    = 6;

        $data['search']         = $this->Dokumenriau_m->search();

        if (!empty($cabang) || !empty($capem) || !empty($periodebulan) || !empty($periodetahun) || !empty($rekon) || !empty($medical) || !empty($klaim) || !empty($restitusi)){
            $data['datana']         = $this->Dokumenriau_m->search();
        } else{
            $data['datana']         = $this->Dokumenriau_m->dokumen();
        }
        $this->template->load('template','dokumenriau',$data);
    }

    function update($id_data)
    {
        extract($_POST);
        $date = date("Y-m-d H:i:s");
        $lahir1 = date("Ymd", strtotime($lahir));
        $buka1 = date("Ymd", strtotime($buka));
        $tglasuransi = date("Ymd", strtotime($tglpengajuanasuransi));
        $amount1 = str_replace(",", "", $amount);
        if (!empty($sex)) {
        if ($sex === 'Laki-Laki') {
            $sex = 'L';
        }else if ($sex === 'Perempuan') {
            $sex = 'P';
        } else{
            $sex = '';
        }
         }

         if ($tglasuransi == 19700101) {
            $this->db->query("update PAN_BRK.dbo.DataRiau set cab = '$cab', pk = '$pk', norek = '$norek', nama = '$nama', lahir = '$lahir1', buka = '$buka1', tempo = '$tempo', plankredit = '$plankredit', id = '$id', amount = '$amount1', ktp = '$ktp', rate = '$rate', sex = '$sex', asuransi = '$asuransi', npwp = '$npwp', old_pk = '$old_pk', rate_asuransi = '$rate_asuransi', status_medical = '$status_medical', modifiedby = '".$this->session->userdata('NamaUser')."', date_modified = GETDATE() where id_data = '$id_data'");
            $this->session->set_flashdata('success', 'Update Data Success');
            redirect('dokumenriau');
         } else{
             $this->db->query("update PAN_BRK.dbo.DataRiau set cab = '$cab', pk = '$pk', norek = '$norek', nama = '$nama', lahir = '$lahir1', buka = '$buka1', tempo = '$tempo', plankredit = '$plankredit', id = '$id', amount = '$amount1', ktp = '$ktp', rate = '$rate', sex = '$sex', asuransi = '$asuransi', npwp = '$npwp', old_pk = '$old_pk', rate_asuransi = '$rate_asuransi', status_medical = '$status_medical', tglpengajuanasuransi = '$tglasuransi', modifiedby = '".$this->session->userdata('NamaUser')."', date_modified = GETDATE() where id_data = '$id_data'");
            $this->session->set_flashdata('success', 'Update Data Success');
            redirect('dokumenriau');
         }

         // var_dump($tglpengajuanasu);
         // die();
         
        $this->db->query("update PAN_BRK.dbo.DataRiau set cab = '$cab', pk = '$pk', norek = '$norek', nama = '$nama', lahir = '$lahir1', buka = '$buka1', tempo = '$tempo', plankredit = '$plankredit', id = '$id', amount = '$amount1', ktp = '$ktp', rate = '$rate', sex = '$sex', asuransi = '$asuransi', npwp = '$npwp', old_pk = '$old_pk', rate_asuransi = '$rate_asuransi', status_medical = '$status_medical', tglpengajuanasuransi = '$tglpengajuanasu', modifiedby = '".$this->session->userdata('NamaUser')."', date_modified = GETDATE() where id_data = '$id_data'");
        $this->session->set_flashdata('success', 'Update Data Success');
        redirect('dokumenriau');
    }

    function delete($id_data)
    {
            $this->db->query("update PAN_BRK.dbo.DataRiau set status = '0'
            where id_data = '$id_data'");
            $this->session->set_flashdata('success', 'Delete Data Successfully');
            redirect('dokumenriau');
    }


    public function download($id_data){

        $dok = $this->db->query("select buktiaktifbatal from PAN_BRK.dbo.DataRiau
            where id_data = '$id_data' order by date_created ASC")->row();

        // $res = preg_replace('/\D/', '', $dok[0]['buktipembatalan']);
        $res = $dok->buktiaktifbatal;

        $file_name= 'upload/dokbukti/'.$res;

        $this->load->helper('download');
        $data = file_get_contents($file_name);
        $name = 'BuktiAktifBatal'.$res; // custom file name for your download

        force_download($name, $data);
    }

    public function upload_avatar()
{
    extract($_POST);

    // var_dump($_POST);
    // die();

    if ($this->input->method() === 'post') {

        $file_name = $id_data;
        $config['upload_path']          = FCPATH.'/upload/dokbukti/';
        $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|zip';
        $config['file_name']            = $file_name;
        $config['overwrite']            = true;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('buktipembatalan')) {
            $uploaded_data = $this->upload->data();

            $new_data = [
                'id_data' => $id_data,
                'buktipembatalan' => 'buktibatal'.$uploaded_data['file_name'],
            ];
            
            $new = $new_data['buktipembatalan'];

            $this->db->query("update PAN_BRK.dbo.DataRiau set status = '$aktif', buktipembatalan = '$new' where id_data = '$id_data'");

            $this->session->set_flashdata('success', 'Update Data Success');
            redirect('dokumenriau');
        } else{
            $this->session->set_flashdata('error', 'Update Data Failed');
            redirect('dokumenriau');
        }
    }
}

    public function ajax_list()
    {
        header('Content-Type: application/json');
        $list = $this->Dokumenriau_m->dokumen();
        $data = array();
        $no = 1;
        //looping data mahasiswa
        foreach ($list as $value) {
            $id_data = $value['id_data'];
            $dok = $this->db->query("select id_data, buktiaktifbatal from PAN_BRK.dbo.DataRiau
            where id_data = '$id_data' order by date_created ASC ")->result_array();
            $user = strtoupper($this->session->userdata('KodeUser'));

            $a = $value['cab'];
            $cab = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang
            where id_cabang = '$a'")->result_array();
            foreach ($cab as $key => $cab1) {
                $cab1 = $cab1['nama_cabang'];
            }

            $tanggal = new DateTime($value['lahir']);
            $today = new DateTime('today');
            $y = $today->diff($tanggal)->y;
            $m = $today->diff($tanggal)->m;
            $d = $today->diff($tanggal)->d;
            if ($m >= 6 && $d > 0) {
                $year = $y + 1;
            } else{
                $year = $y;
            }

            $b = $value['plankredit'];
            $plan = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterPlan
            where kode_plan = '$b'")->result_array();
            foreach ($plan as $key => $plan1) {
                $plan1 = $plan1['nama_plan'];
            }

            $id = $value['id'];
            $idkerja = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterDebitur
            where id_pekerjaan = '$id'")->result_array();
            foreach ($idkerja as $key => $idkerja1) {
                $idkerja1 = $idkerja1['nama_pekerjaan'];
            }

            if ($value['sex'] == 'P'){
                $sex = 'Perempuan';
            } else if ($value['sex'] == 'L') {
                $sex = 'Laki-Laki';
            } else{
                $sex = '';
            }

            if ($value['amount'] != '' && $value['rate_asuransi'] != '') {
                $premis = ($value['amount'] * $value['rate_asuransi']) / 1000;
            }
                if (!empty($premis)) {
                    $premi = number_format(floatval($premis), 0);
                } else{
                    $premi = '0';
                }

            $fbi = str_replace(',', '', $premi) * 0.1;
            $fbis = number_format($fbi);

            $ass = $value['asuransi'];
            $asuransi = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterAsuransi
            where id_asuransi = '$ass'")->result_array();
            if (!empty($asuransi)) {
                foreach ($asuransi as $key => $asu) {
                    $asu = $asu['nama_asuransi'];
                }
            } else {
                $asu = '';
            }

            $typ = $value['id_type'];
            $typemanfaat = $this->db->query("SELECT nama_type FROM PAN_BRK.dbo.TypeManfaat
            where id_type = '$typ'")->result_array();                                
            foreach ($typemanfaat as $key => $type1) {
                $type1 = $type1['nama_type'];
            }
            
            $row = array();
            //row pertama akan kita gunakan untuk btn edit dan delete
            $row[] =  "<a title='Change'
                        href='javascript:;'
                        data-No_Acc='<?php echo $id_data;?>'
                        data-toggle='modal' 
                        data-target='#edit<?php echo $id_data?>'
                        >
                        <button class='btn btn-sm btn-info'>
                            <i class='glyphicon glyphicon-pencil'></i>
                        </button> 
                    </a>
                    <a title='Delete'
                    href='javascript:;'
                    data-No_Acc='<?php echo $id_data;?>'
                    data-toggle='modal' 
                    data-target='#batal<?php echo $id_data?>'
                    >
                    <button class='btn btn-sm btn-danger'>
                        <i class='glyphicon glyphicon-remove'></i>
                    </button> 
                </a>&nbsp;";
            $row[] = $no++;
            $row[] = $cab1;
            $row[] = $value['pk'];
            $row[] = $value['norek'];
            $row[] = $value['nama'];
            $row[] = date("d/m/Y", strtotime($value['lahir'])). ' ('.$year.'th)';
            $row[] = date("d/m/Y", strtotime($value['buka']));
            $row[] = $value['tempo'];
            $row[] = $plan1;
            $row[] = $idkerja1;
            $row[] = number_format($value['amount'], 0);
            $row[] = $value['ktp'];
            $row[] = $value['rate'];
            $row[] = $sex;
            $row[] = $value['rate_asuransi'];
            $row[] = $premi;
            $row[] = $fbis;
            $row[] = $asu;
            $row[] = $type1;
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->post('draw'),
            // "recordsTotal" => $this->Dokumenriau_m->count_all(),
            // "recordsFiltered" => $this->Dokumenriau_m->count_filtered(),
            "data" => $data,
        );
        //output to json format
        $this->output->set_output(json_encode($output));
    }


}