<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//contoh callback yang bisa dipanggil 
class Klaimapi extends CI_Controller {

    function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $this->getdatariau();
    }

    public function getdatariau(){

        $data = $this->input->raw_input_stream;
        // var_dump($data);
        // die();
        $data_json = json_decode($data, true); 

        var_dump($data_json);
        die();

        //selanjutnya lakukan proses data sesuai sesuai kebutuhan
        $success = true;
        if(!empty($data_json)){

            // foreach($data_json as $idx => $dt){
                $arr_insert = array(
                    'id_transaksi' => $data_json['id_transaksi'],
                        'kode_broker' => $data_json['kode_broker'],
                        'kode_cabang' => $data_json['kode_cabang'],
                        'no_pk' => $data_json['no_pk'],
                        'nomor_rekening' => $data_json['nomor_rekening'],
                        'nama' => $data_json['nama'],
                        'tenor' => $data_json['tenor'],
                        'ktp' => $data_json['ktp'],
                        'periode_awal' => $data_json['periode_awal'],
                        'periode_akhir' => $data_json['periode_akhir'],
                        'tenor_berjalan' => $data_json['tenor_berjalan'],
                        'sisa_tenor' => $data_json['sisa_tenor'],
                        'jenis_klaim' => $data_json['jenis_klaim'],
                        'tanggal_kirim' => $data_json['tanggal_kirim'],
                        'penyebab_klaim' => $data_json['penyebab_klaim'],
                        'tanggal_kejadian' => $data_json['tanggal_kejadian'],
                        'tempat_kejadian' => $data_json['tempat_kejadian'],
                        'jumlah_diajukan' => $data_json['jumlah_diajukan'],
                        'tujuan_pembayaran' => $data_json['tujuan_pembayaran'],
                        'premi' => $data_json['premi'],
                );
                // var_dump($arr_insert);
                // die();
                $insert = $this->db->query("insert into PAN_BRK.dbo.PengajuanKlaim (
                id_transaksi,
                 kode_broker,
                 kode_cabang,
                 no_pk,
                 nomor_rekening,
                 nama,
                 tenor,
                 ktp,
                 periode_awal,
                 periode_akhir,
                 tenor_berjalan,
                 sisa_tenor,
                 jenis_klaim,
                 tanggal_kirim,
                 penyebab_klaim,
                 tanggal_kejadian,
                 tempat_kejadian,
                 jumlah_diajukan,
                 tujuan_pembayaran,
                 premi
                 ) 
                 values (
                 '".$arr_insert['id_transaksi']."',
                 '".$arr_insert['kode_broker']."',
                 '".$arr_insert['kode_cabang']."',
                 '".$arr_insert['no_pk']."',
                 '".$arr_insert['nomor_rekening']."',
                 '".$arr_insert['nama']."',
                 '".$arr_insert['tenor']."',
                 '".$arr_insert['ktp']."',
                 '".$arr_insert['periode_awal']."',
                 '".$arr_insert['periode_akhir']."',
                 '".$arr_insert['tenor_berjalan']."',
                 '".$arr_insert['sisa_tenor']."',
                 '".$arr_insert['jenis_klaim']."',
                 '".$arr_insert['tanggal_kirim']."',
                 '".$arr_insert['penyebab_klaim']."',
                 '".$arr_insert['tanggal_kejadian']."',
                 '".$arr_insert['tempat_kejadian']."',
                 '".$arr_insert['jumlah_diajukan']."',
                 '".$arr_insert['tujuan_pembayaran']."',
                 '".$arr_insert['premi']."'
                )");

                if($insert === false) $success = false;
        }

        if($success){
            $return = array(
                'status'    => '200',
                'message' => 'Data sudah di input'
            );
            echo json_encode($return);
            die;
        }else{
            $return = array(
                'status'    => '404',
                'message' => 'You dont have permission to access this service'
            );
            echo json_encode($return);
            die;
        }
    }

}
