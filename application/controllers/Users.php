<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Master_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

	function index()
	{
        $data['title']  		= "Users";
        $data['sub_menu']       = 27;
        $data['page_id']        = 28;

        $data['datana']         = $this->Master_m->user_not_exist();
        
		$this->template->load('template','users',$data);
	}

    
}

