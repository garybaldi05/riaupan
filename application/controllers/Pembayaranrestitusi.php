<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaranrestitusi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Pembayaranrestitusi_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Daftar Pembayaran Restitusi';
        $data['sub_menu']   = 23;
        $data['page_id']    = 15;

        $data['datana']         = $this->Pembayaranrestitusi_m->dokumen();

            $this->template->load('template','pembayaranrestitusi',$data);
    }

     function get_capem()
    {
        $id=$this->input->post('id');
        $data=$this->Pembayaranrestitusi_m->get_capem($id);
        echo json_encode($data);
    }

    function search()
    {

        extract($_POST);

        $data['title']      = 'Daftar Pembayaran Restitusi';
        $data['sub_menu']   = 23;
        $data['page_id']    = 15;

        $data['search']         = $this->Pembayaranrestitusi_m->search();

        if (!empty($cabang) || !empty($capem) || !empty($periodebulan) || !empty($periodetahun)){
            $data['datana']         = $this->Pembayaranrestitusi_m->search();
        } else{
            $data['datana']         = $this->Pembayaranrestitusi_m->dokumen();
        }
        $this->template->load('template','pembayaranrestitusi',$data);
    }

}