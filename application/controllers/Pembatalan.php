<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembatalan extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Pembatalan_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Pembatalan Asuransi';
        $data['sub_menu']   = 21;
        $data['page_id']    = 29;

        $data['datana']         = $this->Pembatalan_m->dokumen();

            $this->template->load('template','pembatalan',$data);
    }

    function get_capem()
    {
        $id=$this->input->post('id');
        $data=$this->Pembatalan->get_capem($id);
        echo json_encode($data);
    }

    function search()
    {

        extract($_POST);

        $data['title']      = 'Pembatalan Asuransi';
        $data['sub_menu']   = 21;
        $data['page_id']    = 29;

        $data['search']         = $this->Pembatalan_m->search();

        if (!empty($cabang) || !empty($capem) || !empty($periodebulan) || !empty($periodetahun) || !empty($rekon) || !empty($medical) || !empty($klaim) || !empty($restitusi)){
            $data['datana']         = $this->Pembatalan_m->search();
        } else{
            $data['datana']         = $this->Pembatalan_m->dokumen();
        }
        $this->template->load('template','pembatalan',$data);
    }

        public function upload_avatar()
{
    extract($_POST);

    if ($this->input->method() === 'post') {

        // var_dump($)
        // the user id contain dot, so we must remove it
        $file_name = $id_data;
        $config['upload_path']          = FCPATH.'/upload/dokbukti/';
        $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|zip';
        $config['file_name']            = $file_name;
        $config['overwrite']            = true;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('buktiaktifbatal')) {
            $uploaded_data = $this->upload->data();

            $new_data = [
                'id_data' => $id_data,
                'buktiaktifbatal' => 'buktiaktifbatal'.$uploaded_data['file_name'],
            ];
            
            $new = $new_data['buktiaktifbatal'];

            $this->db->query("update PAN_BRK.dbo.DataRiau set status = '$aktif', buktiaktifbatal = '$new' where id_data = '$id_data'");

            $this->session->set_flashdata('success', 'Update Data Success');
            redirect('pembatalan');
        } else{
            $this->session->set_flashdata('error', 'Update Data Failed');
            redirect('pembatalan');
        }
    }
}

public function download($id_data){

        $dok = $this->db->query("select buktipembatalan from PAN_BRK.dbo.DataRiau
            where id_data = '$id_data' order by date_created ASC")->row();

        // var_dump($dok);
        // die();

        // $res = preg_replace('/\D/', '', $dok[0]['buktipembatalan']);
        $res = $dok->buktipembatalan;

        $file_name= 'upload/dokbukti/'.$res;

        $this->load->helper('download');
        $data = file_get_contents($file_name);
        $name = 'BuktiPembatalan_'.$res; // custom file name for your download

        force_download($name, $data);
    }

}