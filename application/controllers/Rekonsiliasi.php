<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekonsiliasi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Rekonsiliasi_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Rekonsiliasi';
        $data['sub_menu']   = 21;
        $data['page_id']    = 18;

        // $data['datana']         = $this->Rekonsiliasi_m->dokumen();

        if (empty($this->uri->segment(3))) {
          $perpage = 0;
        } else{
        $perpage = $this->uri->segment(3);
        }
          $offset = 10;
         $data['datana'] = $this->Rekonsiliasi_m->getDataPagination($perpage, $offset);

         $config['base_url'] = site_url('rekonsiliasi/index');
         $config['total_rows'] = $this->Rekonsiliasi_m->dokumen()->num_rows();
         $config['per_page'] = $offset;

         $config['next_link'] = 'Next';
          $config['prev_link'] = 'Previous';
          $config['first_link'] = 'First';
          $config['last_link'] = 'Last';
          $config['full_tag_open'] = '<ul class="pagination">';
          $config['full_tag_close'] = '</ul>';
          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
          $config['cur_tag_open'] = '<li class="active"><a href="#">';
          $config['cur_tag_close'] = '</a></li>';
          $config['prev_tag_open'] = '<li>';
          $config['prev_tag_close'] = '</li>';
          $config['next_tag_open'] = '<li>';
          $config['next_tag_close'] = '</li>';
          $config['last_tag_open'] = '<li>';
          $config['last_tag_close'] = '</li>';
          $config['first_tag_open'] = '<li>';
          $config['first_tag_close'] = '</li>';

         $this->pagination->initialize($config);

            $this->template->load('template','rekonsiliasi',$data);
    }

    function get_capem()
    {
        $id=$this->input->post('id');
        $data=$this->Rekonsiliasi_m->get_capem($id);
        echo json_encode($data);
    }

    function search()
    {

        extract($_POST);

        $data['title']      = 'Rekonsiliasi';
        $data['sub_menu']   = 21;
        $data['page_id']    = 18;

        $data['search']         = $this->Rekonsiliasi_m->search();

        if (!empty($cabang) || !empty($capem) || !empty($periodebulan) || !empty($periodetahun)){
            $data['datana']         = $this->Rekonsiliasi_m->search();
        } else{
            $data['datana']         = $this->Rekonsiliasi_m->dokumen();
        }
        $this->template->load('template','rekonsiliasi',$data);
    }

    function update($id_data)
    {
        extract($_POST);

        // var_dump($_POST);
        // die();
        
        $date = date("Y-m-d H:i:s");
        $tglbayarrk = date("Y-m-d", strtotime($tglbayarrk));
         
        $this->db->query("update PAN_BRK.dbo.DataRiau set tglbayarrk = '$tglbayarrk', jumlah_bayar = '$jumlahbayar', status_rekon = '1',
            createdby_rekon = '".$this->session->userdata('NamaUser')."', createddate_rekon = GETDATE() where id_data = '$id_data'");

        $this->db->query("insert into PAN_BRK.dbo.Histori_Rekon 
                (id_data, nama, norek, pk, jenis_pembayaran, amount, rate_asuransi, jumlahpembayaran, tglpembayaran, createdby, createddate) 
                values ('$id_data', '$nama', '$norek', '$pk', 'Pembayaran Bank', '$amount', '$rate_asuransi', '$jumlahbayar', '$tglbayarrk', '".$this->session->userdata('NamaUser')."', GETDATE())");

        $this->session->set_flashdata('success', 'Rekonsiliasi Data Sukses');
        redirect('rekonsiliasi');
    }

}