<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekappenutupanasuransi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Rekappenutupanasuransi_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Rekap Penutupan Asuransi';
        $data['sub_menu']   = 21;
        $data['page_id']    = 7;

        // $data['datana']         = $this->Rekappenutupanasuransi_m->dokumen();

        if (empty($this->uri->segment(3))) {
          $perpage = 0;
        } else{
        $perpage = $this->uri->segment(3);
        }
          $offset = 10;
         $data['datana'] = $this->Rekappenutupanasuransi_m->getDataPagination($perpage, $offset);

         $config['base_url'] = site_url('rekappenutupanasuransi/index');
         $config['total_rows'] = $this->Rekappenutupanasuransi_m->dokumen()->num_rows();
         $config['per_page'] = $offset;

         $config['next_link'] = 'Next';
          $config['prev_link'] = 'Previous';
          $config['first_link'] = 'First';
          $config['last_link'] = 'Last';
          $config['full_tag_open'] = '<ul class="pagination">';
          $config['full_tag_close'] = '</ul>';
          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
          $config['cur_tag_open'] = '<li class="active"><a href="#">';
          $config['cur_tag_close'] = '</a></li>';
          $config['prev_tag_open'] = '<li>';
          $config['prev_tag_close'] = '</li>';
          $config['next_tag_open'] = '<li>';
          $config['next_tag_close'] = '</li>';
          $config['last_tag_open'] = '<li>';
          $config['last_tag_close'] = '</li>';
          $config['first_tag_open'] = '<li>';
          $config['first_tag_close'] = '</li>';

         $this->pagination->initialize($config);

            $this->template->load('template','rekappenutupanasuransi',$data);
    }

    function get_capem()
    {
        $id=$this->input->post('id');
        $data=$this->Rekappenutupanasuransi_m->get_capem($id);
        echo json_encode($data);
    }

    function search()
    {

        extract($_POST);

        $data['title']      = 'Rekap Penutupan Asuransi';
        $data['sub_menu']   = 21;
        $data['page_id']    = 7;

        $data['search']         = $this->Rekappenutupanasuransi_m->search();

        if (!empty($cabang) || !empty($capem) || !empty($periodebulan) || !empty($periodetahun)){
            $data['datana']         = $this->Rekappenutupanasuransi_m->search();
        } else{
            $data['datana']         = $this->Rekappenutupanasuransi_m->dokumen();
        }
        $this->template->load('template','rekappenutupanasuransi',$data);
    }

    public function export(){
        extract($_POST);

        $date = date("d-m-Y H:i:s");

        $data['title']      = 'Rekap Penutupan Asuransi '. $date;
        $data['sub_menu']   = 21;
        $data['page_id']    = 7;

        if (!empty($capem)) {
            $capems = $capem;
        } else{
            $capems = '';
        }

        $data['cabs']      = $cabang;
        $data['caps']      = $capems;
        $data['periodebulans']      = $periodebulan;
        $data['periodetahuns']      = $periodetahun;

        $data['search']         = $this->Rekappenutupanasuransi_m->search();

        if (!empty($cabang) || !empty($capem) || !empty($periodebulan) || !empty($periodetahun)){
            $data['datana1']         = $this->Rekappenutupanasuransi_m->search();
        } else{
            $data['datana1']         = $this->Rekappenutupanasuransi_m->dokumen();
        }

        // var_dump($_POST);
        // die();

        $this->load->view('reportpenutupan',$data);
      }

}