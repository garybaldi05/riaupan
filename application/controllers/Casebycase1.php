<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Casebycase extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Casebycase_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Case by Case';
        $data['sub_menu']   = 21;
        $data['page_id']    = 20;

        $data['datana']         = $this->Casebycase_m->dokumen();

            $this->template->load('template','casebycase',$data);
    }

    function get_capem()
    {
        $id=$this->input->post('id');
        $data=$this->Casebycase_m->get_capem($id);
        echo json_encode($data);
    }

    function search()
    {

        extract($_POST);

        $data['title']      = 'Case by Case';
        $data['sub_menu']   = 21;
        $data['page_id']    = 20;

        $data['search']         = $this->Casebycase_m->search();

        if (!empty($cabang) || !empty($capem) || !empty($periodebulan) || !empty($periodetahun)){
            $data['datana']         = $this->Casebycase_m->search();
        } else{
            $data['datana']         = $this->Casebycase_m->dokumen();
        }
        $this->template->load('template','casebycase',$data);
    }

}