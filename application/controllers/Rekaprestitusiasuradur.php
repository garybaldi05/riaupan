<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekaprestitusiasuradur extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_m');
        $this->load->model('Rekaprestitusiasuradur_m');

        if(!$this->Login_m->logged_id())
        {
            session_destroy();
            redirect('login');         
        }
    }

    function index()
    {
        $data['title']      = 'Rekap Restitusi Asuradur';
        $data['sub_menu']   = 23;
        $data['page_id']    = 17;

        $data['datana']         = $this->Rekaprestitusiasuradur_m->dokumen();

            $this->template->load('template','rekaprestitusiasuradur',$data);
    }

}