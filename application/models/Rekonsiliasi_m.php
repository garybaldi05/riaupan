<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekonsiliasi_m extends CI_Model
{

    public function dokumen()
    {

        $dok = $this->db->query("select * from PAN_BRK.dbo.DaftarUser
            where KodeUser = '".$this->session->userdata('KodeUser')."' ")->result_array();

        if (!empty($dok)) {
            $dokumen = $dok[0]['NamaUser'];
        } else{
            $dokumen = '';
        }

        // var_dump($dokumen);
        // die();

        $q = "select * from PAN_BRK.dbo.DataRiau a 
            join PAN_BRK.dbo.MasterCabang b on a.cab = b.id_cabang
            where 
                cab != ''
                and pk != ''
                and norek != ''
                and nama != ''
                and lahir != ''
                and buka != ''
                and tempo != ''
                and plankredit != ''
                and id != ''
                and ktp != ''
                and rate != ''
                and sex != ''
                and npwp != '' ";

        if (strpos($dokumen, 'Cabang') !== false){
                $q .= " and b.id_induk = '".$dok[0]['id_cabang']."' ";
        } elseif (strpos($dokumen, 'Capem') !== false) {
            $q .= " and b.id_cabang = '".$dok[0]['id_cabang']."' ";
        } elseif (strpos($dokumen, 'Kedai') !== false) {
            $q .= " and b.id_cabang = '".$dok[0]['id_cabang']."' ";
        }

        $q .= "order by date_created DESC";

        $result = $this->db->query($q);
        return $result;
    }

    public function getDataPagination($limit, $offset)
    {
         $query = $this->db->query("select * from PAN_BRK.dbo.DataRiau order by date_created OFFSET $limit ROWS FETCH NEXT $offset ROWS ONLY");
        return $query->result_array();
    }

    function get_capem($id)
    {
        $query = $this->db->query("select * from PAN_BRK.dbo.MasterCabang
		where id_induk = '$id'
		order by id_induk ASC")->result();
        return $query;
    }

    public function search()
    {
    	extract($_POST);

    	$periode = $periodetahun.$periodebulan;
        $q = "select * from PAN_BRK.dbo.DataRiau where 
                pk != ''
                and norek != ''
                and nama != ''
                and lahir != ''
                and tempo != ''
                and plankredit != ''
                and id != ''
                and ktp != ''
                and rate != ''
                and sex != ''
                and npwp != '' ";

        if (!empty($capem)){
            $q .= " and cab='$capem' ";
        } else{
        	$q .= " and cab != '' ";
        }

        if (!empty($periodebulan) and !empty($periodetahun)){
            $q .= " and LEFT(buka, 6) = '$periode' ";
        } else{
        	$q .= " and buka != '' ";
        }

        if (!empty($periodebulan)){
            $q .= " and SUBSTRING(buka,5,2) = '$periodebulan' ";
        } else{
        	$q .= " and buka != '' ";
        }

        if (!empty($periodetahun)){
            $q .= " and LEFT(buka, 4) = '$periodetahun' ";
        } else{
        	$q .= " and buka != '' ";
        }

        $q .= "order by cab ASC";

        $result = $this->db->query($q);
        return $result->result_array();
    }
}