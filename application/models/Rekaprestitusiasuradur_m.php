<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekaprestitusiasuradur_m extends CI_Model
{

    public function dokumen()
    {
    	$res = $this->db->query("select * from PAN_BRK.dbo.DataRiau where 
				kodeh2h != ''
				and password != ''
				and userid != ''
				and cab != ''
				and pk != ''
				and norek != ''
				and nama != ''
				and lahir != ''
				and buka != ''
				and tempo != ''
				and plankredit != ''
				and id != ''
				and ktp != ''
				and rate != ''
				and sex != ''
				and npwp != ''
				and old_pk != ''")->result_array();

    	if (!empty($res)) {
			$restitusi = $res[0]['old_pk'];
		} else{
			$restitusi = '';
		}

        $query = $this->db->query("select * from PAN_BRK.dbo.DataRiau where 
				kodeh2h != ''
				and password != ''
				and userid != ''
				and cab != ''
				and pk != ''
				and norek != ''
				and nama != ''
				and lahir != ''
				and buka != ''
				and tempo != ''
				and plankredit != ''
				and id != ''
				and ktp != ''
				and rate != ''
				and sex != ''
				and npwp != ''
				and pk = '$restitusi'");
        return $query->result_array();
    }

    public function search()
    {
    	extract($_POST);

    	$periode = $periodetahun.$periodebulan;
    	$res = $this->db->query("select * from PAN_BRK.dbo.DataRiau where 
				kodeh2h != ''
				and password != ''
				and userid != ''
				and cab != ''
				and pk != ''
				and norek != ''
				and nama != ''
				and lahir != ''
				and buka != ''
				and tempo != ''
				and plankredit != ''
				and id != ''
				and ktp != ''
				and rate != ''
				and sex != ''
				and npwp != ''
				and old_pk != ''");

    	if (!empty($res)) {
			$restitusi = $res[0]['old_pk'];
		} else{
			$restitusi = '';
		}

        $q = "select * from PAN_BRK.dbo.DataRiau where 
				kodeh2h != ''
				and password != ''
				and userid != ''
				and cab != ''
				and pk != ''
				and norek != ''
				and nama != ''
				and lahir != ''
				and buka != ''
				and tempo != ''
				and plankredit != ''
				and id != ''
				and ktp != ''
				and rate != ''
				and sex != ''
				and npwp != ''
				and pk = '$restitusi' ";

        if (!empty($capem)){
            $q .= " and cab='$capem' ";
        } else{
        	$q .= " and cab != '' ";
        }

        if (!empty($periodebulan) and !empty($periodetahun)){
            $q .= " and LEFT(buka, 6) = '$periode' ";
        } else{
        	$q .= " and buka != '' ";
        }

        if (!empty($periodebulan)){
            $q .= " and SUBSTRING(buka,5,2) = '$periodebulan' ";
        } else{
        	$q .= " and buka != '' ";
        }

        if (!empty($periodetahun)){
            $q .= " and LEFT(buka, 4) = '$periodetahun' ";
        } else{
        	$q .= " and buka != '' ";
        }

        $q .= "order by cab ASC";

        $result = $this->db->query($q);
        return $result->result_array();
    }
}