<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datakerjadebitur_m extends CI_Model
{

    public function dokumens()
    {
        $query = $this->db->query("select * from PAN_BRK.dbo.JenisDebitur");
        return $query->result_array();
    }
}