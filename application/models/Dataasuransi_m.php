<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataasuransi_m extends CI_Model
{

    public function dataasuransi()
    {
        $query = $this->db->query("select * from PAN_BRK.dbo.MasterAsuransi");
        return $query->result_array();
    }
}