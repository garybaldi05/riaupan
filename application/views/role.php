 <div class="container-full">
		<!-- Main content -->
		<section class="content">
			<div class="row">
			<div class="col-12">
			  <div class="box box-default">
			  	<div class="box-header with-border">
                  <h4 class="box-title">Role Menu</h4>
                </div>
				<!-- /.box-header -->
				<div class="box-body">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs customtab" role="tablist">
						<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home2" role="tab"><span class="hidden-sm-up"><i class="ion-home"></i></span> <span class="hidden-xs-down">Data</span></a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile2" role="tab"><span class="hidden-sm-up"><i class="ion-person"></i></span> <span class="hidden-xs-down">Tambah</span></a> </li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<br>
						<?php
			            if($this->session->flashdata('success')){
			                ?>
			                <div class="alert alert-success text-center">
			                    <i class="glyphicon glyphicon-ok-sign"></i> <span><?=$this->session->flashdata('success')?></span>
			                </div>
			                <div></div>
			                <?php
			            }
			            ?>
			            <?php
			            if($this->session->flashdata('error')){
			                ?>
			                <div class="alert alert-danger text-center">
			                    <i class="glyphicon glyphicon-remove-sign"></i> <span><?=$this->session->flashdata('error')?></span>
			                </div>
			                <div>
			                </div>
			            <?php   } ?>
						<div class="tab-pane active" id="home2" role="tabpanel">
						<div class="box">
				<div class="box-header with-border">
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Nama User :</label>
                                    <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder="Nama User">
                                </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Nama Menu :</label>
                                    <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder="Nama Menu">
                                	</div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="button" class="waves-effect waves-light btn btn-rounded btn-primary-light mb-5"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
                                </div>
                            </div>
                        </div>
                        <br>

					<div class="table-responsive">
					  <table id="example1" class="table">
						<thead class="bg-dark">
							<tr>
								<th>Nama User</th>
								<th>Nama Menu</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php  foreach ($datana as $key => $value) { ?>
                            <tr id="<?php echo $value['id_role']; ?>">
                                <td class="text-center"><?=$value['NamaUser']?></td>
                                <td class="text-center"><?=$value['nama_menu']?></td>
                                <td class="text-center">
                                    <a href="<?php echo base_url('menu/delete/'.$value['id_role'])?>" onclick="return confirm('Are your sure ?');" class="btn-sm btn-danger remove">
                                        <i class="glyphicon glyphicon-remove"></i>
                                    </a>
                                </td>
                            </tr>  
                            <?php } ?>
						</tbody>
						<tfoot>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
				</div>
				<div class="tab-pane" id="profile2" role="tabpanel">
				<div class="box box-default">
			<div class="box-header with-border">
			</div>
			<!-- /.box-header -->

			<<form action="<?=base_url('menu/save')?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" >
				  <div class="box-body">
					<div class="form-group row">
                            <label class="col-sm-2 control-label" for="example-hf-email">User</label>
                            <div class="col-sm-10">
                                <select name="KodeUser" class="custom-select form-control" style="width: 100%;">
                                    <?php
                                    $user = $this->Master_m->user_not_exist();
                                        foreach ($user as $value) {
                                            echo"<option value='".$value['KodeUser']."'>".$value['NamaUser']."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 control-label" for="example-hf-email">Role</label>
                            <div class="col-lg-3 col-sm-3 col-xs-12">
                                 <select name="from" id="multiselect" class="form-control" size="8" multiple="multiple">
                                    <?php
                                    $menu = $this->Master_m->menu();
                                        foreach ($menu as $value) {
                                            if ($value['kat_menu'] == 0 ) {
                                                echo"<option style='font-weight:bold;' value='".$value['id_menu']."'>".$value['nama_menu']."</option>";
                                            } else {
                                                echo"<option value='".$value['id_menu']."'>".$value['nama_menu']."</option>";
                                            }
                                            
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="multiselect-controls col-lg-1 col-sm-1 col-xs-6">
                                <button type="button" id="multiselect_rightAll" class="btn btn-block"><i class="glyphicon glyphicon-fast-forward"></i></button>
                                <button type="button" id="multiselect_rightSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
                                <button type="button" id="multiselect_leftSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
                                <button type="button" id="multiselect_leftAll" class="btn btn-block"><i class="glyphicon glyphicon-fast-backward"></i></button>
                              </div>

                              <div class="col-lg-3 col-sm-3 col-xs-12">
                                <select name="to[]" id="multiselect_to" class="form-control" size="8" multiple="multiple">

                                </select>
                              </div>
                        </div>
				  </div>
				  <!-- /.box-body -->
				  <div class="box-footer">
					<button type="submit" class="btn btn-rounded btn-danger">Cancel</button>
					<button type="submit" class="btn btn-rounded btn-info pull-right">Save</button>
				  </div>
				  <!-- /.box-footer -->
				</form>
		  </div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			</div>		
		</section>

		<script type="text/javascript" src="<?php echo base_url();?>assets/main/js/jquery-1.7.1.min.js"></script>
    	<script type="text/javascript" src="<?php echo base_url();?>assets/main/js/multiselect.js"></script>
		<!-- /.content -->
	  </div>