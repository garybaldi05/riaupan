 <div class="container-full">
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-12">
					<div class="box bg-gradient-primary overflow-hidden pull-up">
						<div class="box-body pr-0 pl-lg-50 pl-15 py-0">							
							<div class="row align-items-center">
								<div class="col-12 col-lg-8">
									<h1 class="font-size-40 text-white">Welcome <?php echo $this->session->userdata('NamaUser');?> ..</h1>
									<p class="text-white mb-0 font-size-20">
										Work hard in silence, let your success be your noise..
									</p>
								</div>
								<div class="col-12 col-lg-4"><img src="https://eduadmin-template.multipurposethemes.com/bs4/images/svg-icon/color-svg/custom-15.svg" alt=""></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xl-3 col-md-6 col-12">
					<div class="box pull-up">
						<div class="box-body">	
							<div class="bg-primary rounded">
								<h5 class="text-white text-center p-10">Penutupan</h5>
							</div>
							<h1 class="countnm font-size-50 m-0 text-center">
								<?php
								$as = strtoupper($this->session->userdata('KodeUser'));
	                              $sub = substr($as,4);
	                            $user = $this->db->query("select * from PAN_BRK.dbo.DaftarUser
	                                     where KodeUser = '$as' ")->result_array();
	                            if (strpos($as, 'BRK') !== false) {
	                            $cabangs = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
	                            }
	                            if (strpos($as, 'BRK') === false) {
	                                $sql = $this->db->query("SELECT * FROM PAN_BRK.dbo.DataRiau where 
										status = 1
										and kodeh2h != ''
										and password != ''
										and userid != ''
										and cab != ''
										and pk != ''
										and norek != ''
										and nama != ''
										and lahir != ''
										and buka != ''
										and tempo != ''
										and plankredit != ''
										and id != ''
										and ktp != ''
										and sex != ''
										and npwp != '' ");
	                                echo $sql = count($sql->result());
	                            } elseif(!empty($cabangs)){
	                            	$caba = $this->db->query("SELECT * FROM PAN_BRK.dbo.DataRiau a
										join PAN_BRK.dbo.MasterCabang b on b.id_cabang = a.cab
										where 
										b.id_induk = '$sub'
										and a.status = 1
										and kodeh2h != ''
										and password != ''
										and userid != ''
										and cab != ''
										and pk != ''
										and norek != ''
										and nama != ''
										and lahir != ''
										and buka != ''
										and tempo != ''
										and plankredit != ''
										and id != ''
										and ktp != ''
										and sex != ''
										and npwp != ''");
	                            	echo $sql = count($caba->result());
	                            } else{
                                    $sqlss = $this->db->query("SELECT * FROM PAN_BRK.dbo.DataRiau where 
										status = 1
										and kodeh2h != ''
										and password != ''
										and userid != ''
										and cab = '$sub'
										and pk != ''
										and norek != ''
										and nama != ''
										and lahir != ''
										and buka != ''
										and tempo != ''
										and plankredit != ''
										and id != ''
										and ktp != ''
										and sex != ''
										and npwp != ''");
                                    echo $sql = count($sqlss->result());
	                            }
								?>
							</h1>
							<div class="text-center pull-up">
								<a href="<?php echo base_url('dokumenriau')?>" class="btn btn-block btn-primary-light">View all</a>
							</div>			
						</div>					
					</div>
				</div>
				<div class="col-xl-3 col-md-6 col-12">
					<div class="box pull-up">
						<div class="box-body">	
							<div class="bg-warning rounded">
								<h5 class="text-white text-center p-10">Pembatalan</h5>
							</div>
							<h1 class="countnm font-size-50 m-0 text-center">
								<?php
								$as = strtoupper($this->session->userdata('KodeUser'));
	                              $sub = substr($as,4);
	                            $user = $this->db->query("select * from PAN_BRK.dbo.DaftarUser
	                                     where KodeUser = '$as' ")->result_array();
	                            if (strpos($as, 'BRK') !== false) {
	                            $cabangs = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
	                            }
	                            if (strpos($as, 'BRK') === false) {
	                                $sql = $this->db->query("SELECT * FROM PAN_BRK.dbo.DataRiau where 
										status = 0
										and kodeh2h != ''
										and password != ''
										and userid != ''
										and cab != ''
										and pk != ''
										and norek != ''
										and nama != ''
										and lahir != ''
										and buka != ''
										and tempo != ''
										and plankredit != ''
										and id != ''
										and ktp != ''
										and sex != ''
										and npwp != '' ");
	                                echo $sql = count($sql->result());
	                            } elseif(!empty($cabangs)){
	                            	$caba = $this->db->query("SELECT * FROM PAN_BRK.dbo.DataRiau a
										join PAN_BRK.dbo.MasterCabang b on b.id_cabang = a.cab
										where 
										b.id_induk = '$sub'
										and a.status = 0
										and kodeh2h != ''
										and password != ''
										and userid != ''
										and cab != ''
										and pk != ''
										and norek != ''
										and nama != ''
										and lahir != ''
										and buka != ''
										and tempo != ''
										and plankredit != ''
										and id != ''
										and ktp != ''
										and sex != ''
										and npwp != ''");
	                            	echo $sql = count($caba->result());
	                            } else{
                                    $sqlss = $this->db->query("SELECT * FROM PAN_BRK.dbo.DataRiau where 
										status = 0
										and kodeh2h != ''
										and password != ''
										and userid != ''
										and cab = '$sub'
										and pk != ''
										and norek != ''
										and nama != ''
										and lahir != ''
										and buka != ''
										and tempo != ''
										and plankredit != ''
										and id != ''
										and ktp != ''
										and sex != ''
										and npwp != ''");
                                    echo $sql = count($sqlss->result());
	                            }
								?>
							</h1>
							<div class="text-center pull-up">
								<a href="<?php echo base_url('pembatalan')?>" class="btn btn-block btn-primary-light">View all</a>
							</div>										
						</div>					
					</div>
				</div>
				<div class="col-xl-3 col-md-6 col-12">
					<div class="box pull-up">
						<div class="box-body">	
							<div class="bg-danger rounded">
								<h5 class="text-white text-center p-10">Klaim</h5>
							</div>
							<h1 class="countnm font-size-50 m-0 text-center">0</h1>
							<div class="text-center pull-up">
								<a href="<?php echo base_url('pengajuanklaim')?>" class="btn btn-block btn-primary-light">View all</a>
							</div>										
						</div>					
					</div>
				</div>
				<div class="col-xl-3 col-md-6 col-12">
					<div class="box pull-up">
						<div class="box-body">	
							<div class="bg-info rounded">
								<h5 class="text-white text-center p-10">Restitusi</h5>
							</div>
							<h1 class="countnm font-size-50 m-0 text-center">
								<?php
								$q = $this->db->query("select old_pk from PAN_BRK.dbo.DataRiau where 
									kodeh2h != ''
									and password != ''
									and userid != ''
									and cab != ''
									and pk != ''
									and norek != ''
									and nama != ''
									and lahir != ''
									and buka != ''
									and tempo != ''
									and plankredit != ''
									and amount != ''
									and id != ''
									and ktp != ''
									and rate != ''
									and sex != ''
									and rate_asuransi != ''
									and asuransi != ''
									and npwp != ''
									and old_pk != ''")->result_array();

							if (!empty($q)) {
								$restitusi = $q[0]['old_pk'];
							} else{
								$restitusi = '';
							}
					    	
					        $query = $this->db->query("select * from PAN_BRK.dbo.DataRiau where 
									kodeh2h != ''
									and password != ''
									and userid != ''
									and cab != ''
									and pk != ''
									and norek != ''
									and nama != ''
									and lahir != ''
									and buka != ''
									and tempo != ''
									and plankredit != ''
									and amount != ''
									and id != ''
									and ktp != ''
									and sex != ''
									and npwp != ''
									and pk = '$restitusi'");
								echo $a = count($query->result());
								?>
							</h1>
							<div class="text-center pull-up">
								<a href="<?php echo base_url('pengajuanrestitusi')?>" class="btn btn-block btn-primary-light">View all</a>
							</div>								
						</div>					
					</div>
				</div>
			</div>

		  <div class="row">
			<div class="col-12">
				<div class="box">
					<div class="box-header">
					<h4 class="box-title">Share Realtime</h4>
				</div>
				  <div class="row no-gutters py-2">
					<div class="col-12 col-lg-3">
					  <div class="box-body br-1 border-light">
						<div class="flexbox mb-1">
						  <span>
						  	<div>
						  	<img src="<?php echo base_url()?>assets/images/askrida.ico" alt="" style="width: 60px;">
						  </div>
							  <div>
							ASKRIDA
							</div>
						  </span>
						  <span class="text-primary font-size-40">0%</span>
						</div>
						<div class="progress progress-xxs mt-10 mb-0">
						  <div class="progress-bar" role="progressbar" style="width: 0%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					  </div>
					</div>

					<!-- <div class="col-12 col-lg-3 hidden-down">
					  <div class="box-body br-1 border-light">
						<div class="flexbox mb-1">
						  <span>
						  	<div>
						  	<img src="<?php echo base_url()?>assets/images/jamkrindo.ico" alt="" style="width: 45px;">
							  </div>
							  <div>
							JAMKRINDO
							</div>
						  </span>
						  <span class="text-primary font-size-40">3.5%</span>
						</div>
						<div class="progress progress-xxs mt-10 mb-0">
						  <div class="progress-bar" role="progressbar" style="width: 3.5%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					  </div>
					</div> -->
					<!-- <div class="col-12 col-lg-3 d-none d-lg-block">
					  <div class="box-body br-1 border-light">
						<div class="flexbox mb-1">
						  <span>
						  	<div>
						  	<img src="<?php echo base_url()?>assets/images/askrindo.ico" alt="" style="width: 60px;">
							  </div>
							  <div>
							ASKRINDO
							</div>
						  </span>
						  <span class="text-primary font-size-40">15%</span>
						</div>
						<div class="progress progress-xxs mt-10 mb-0">
						  <div class="progress-bar" role="progressbar" style="width: 15%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					  </div>
					</div> -->
					<!-- <div class="col-12 col-lg-3 d-none d-lg-block">
					  <div class="box-body">
						<div class="flexbox mb-1">
						 <span>
						  	<div>
						  	<img src="<?php echo base_url()?>assets/images/jasindo.ico" alt="" style="width: 60px;">
							  </div>
							  <div>
							JASINDO
							</div>
						  </span>
						  <span class="text-primary font-size-40">2.5%</span>
						</div>
						<div class="progress progress-xxs mt-10 mb-0">
						  <div class="progress-bar" role="progressbar" style="width: 2.5%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					  </div>
					</div> -->


				  </div>
				</div>
			</div>
			<!-- /.col -->

		  </div>

		  <div class="col-12">
			  <div class="box">
						<div class="box-body">
						<div class="box-header with-border">
							<h4 class="box-title">Chart Jumlah Pencairan: Klaim</h4>
						</div>
						<div class="box-body">
							<div id="axis-timezone"></div>
						</div>
						</div>
					</div>
			</div>
		
		</section>
		<!-- /.content -->
	  </div>