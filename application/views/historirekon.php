 <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h4 class="box-title">Data Rekonsiliasi</h4>
                </div>
                <!-- /.box-header -->
                <?php $no=1; foreach ($datana as $key => $value) { ?>
                <div class="box-body">
                    <form class="form-horizontal form-element" method="post" action="<?=base_url('historirekon/submitrekon')?>">
                        

                            <input type="text" class="form-control" id="id_data" name="id_data" readonly="" value="<?= $value['id_data'] ?>" style="display: none">

                            <input type="text" class="form-control" id="amount" name="amount" readonly="" value="<?= $value['amount'] ?>" style="display: none">

                            <input type="text" class="form-control" id="rate_asuransi" name="rate_asuransi" readonly="" value="<?= $value['rate_asuransi'] ?>" style="display: none">

                            <div class="row">
                              <label class="col-sm-2 control-label">No. Rekening</label>

                              <div class="col-sm-10">
                                <input type="text" class="form-control" id="norek" name="norek" readonly="" value="<?= $value['norek'] ?>">
                              </div>
                            </div>
                            <br>

                            <div class="row">
                              <label class="col-sm-2 control-label">No. PK</label>

                              <div class="col-sm-10">
                                <input type="text" class="form-control" id="pk" name="pk" readonly="" value="<?= $value['pk'] ?>">
                              </div>
                            </div>
                            <br>

                            <div class="row">
                              <label class="col-sm-2 control-label">Nama</label>

                              <div class="col-sm-10">
                                <input type="text" class="form-control" id="nama" name="nama" readonly="" value="<?= $value['nama'] ?>">
                              </div>
                            </div>
                            <br>

                            <div class="row">
                              <label class="col-sm-2 control-label">Total yang harus dibayar</label>

                              <div class="col-sm-10">
                                <?php
                                if ($value['amount'] != '' && $value['rate_asuransi'] != '') {
                                    $premis = ($value['amount'] * $value['rate_asuransi']) / 1000;
                                }
                                    if (!empty($premis)) {
                                        $premi = number_format(floatval($premis), 0);
                                    } else{
                                        $premi = '0';
                                    }
                                    
                                ?>
                                <input type="text" class="form-control" id="premi" name="premi" readonly="" value="<?= $premi ?>">
                              </div>
                            </div>
                            <br>

                            <div class="row">
                              <label class="col-sm-2 control-label">Total yang sudah dibayar</label>

                              <div class="col-sm-10">
                                <?php 
                                        if($value['jumlah_bayar'] > 0){
                                        $bayar = str_replace(',', '', $value['jumlah_bayar']);
                                        $jumlah_bayar = number_format($bayar);
                                        } else{
                                           $jumlah_bayar = '0';
                                        }
                                        ?>
                                <input type="text" class="form-control" id="jumlah_bayar" name="jumlah_bayar" readonly="" value="<?= $jumlah_bayar ?>">
                              </div>
                            </div>
                            <br>

                            <div class="row">
                              <label class="col-sm-2 control-label">Selisih Pembayaran</label>

                              <div class="col-sm-10">
                                <?php 
                                $bayar = str_replace(',', '', $value['jumlah_bayar']);
                                if($bayar > 0){
                                    $s = str_replace(',', '', $premi) - $bayar;
                                    $sel = number_format($s);
                                } else{
                                    $sel = '0';
                                }
                                ?>
                                <b><input type="text" class="form-control" id="selisih" name="selisih" readonly="" value="<?= $sel ?>"></b>
                              </div>
                            </div>

                            <div class="box-header with-border"></div>
                            <br>
                            <br>
                            <div class="row">
                              <label class="col-sm-2 control-label">Jenis Pembayaran<span class="text-danger"> *</span></label>

                              <select class="form-control col-sm-10" required="" id="jenis_pembayaran" name="jenis_pembayaran">
                                <option></option>
                                <option value="pembayaranbank">Pembayaran Bank</option>
                                <option value="penarikanbank">Penarikan Bank</option>
                              </select>
                            </div>
                            <br>

                            <div class="row">
                              <label class="col-sm-2 control-label">Jumlah Pembayaran<span class="text-danger"> *</span></label>

                              <div class="col-sm-10">
                                <input type="text" class="form-control" id="jumlahpembayaran" name="jumlahpembayaran" placeholder="0" required="" autocomplete="off">
                              </div>
                            </div>
                            <br>

                            <div class="row">
                              <label class="col-sm-2 control-label">Tanggal Pembayaran<span class="text-danger"> *</span></label>

                              <div class="col-sm-10">
                                <input type="date" class="form-control" id="tglpembayaran" name="tglpembayaran" required="">
                              </div>
                            </div>
                            <br>

                        <!-- <div class="row"> -->
                            <br>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="waves-effect waves-light btn mb-5 bg-gradient-danger">Submit</button>
                                </div>
                            </div>
                        <!-- </div> -->
                        
                    </form>
                  </div>
              </div>
              <?php
                }
                ?>
              <!-- /.box -->
            </div>
            </div>      
        </section>

        <?php
        if($this->session->flashdata('success')){
            ?>
            <div class="alert alert-success text-center">
                <i class="glyphicon glyphicon-ok-sign"></i> <span><?=$this->session->flashdata('success')?></span>
            </div>
            <div></div>
            <?php
        }
        ?>
        <?php
        if($this->session->flashdata('error')){
            ?>
            <div class="alert alert-danger text-center">
                <i class="glyphicon glyphicon-remove-sign"></i> <span><?=$this->session->flashdata('error')?></span>
            </div>
            <div>
            </div>
        <?php   } ?>
        <!-- /.content -->
                <section class="content">
            <div class="row">
                <div class="col-12">
              <div class="box box-default">
                <div class="box-body">
                    <div class="table-responsive">
                      <table id="example1" class="table">
                        <thead class="bg-dark">
                            <tr>
                                <th class="text-center">Tanggal Pembayaran</th>
                                <th class="text-center">Jenis Pembayaran</th>
                                <th class="text-center">Jumlah Pembayaran</th>
                                <th class="text-center"><i>Createdby</i></th>
                                <th class="text-center"><i>Createddate</i></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($datarekon as $key => $val) {?>
                            <tr>
                              <td class="text-center"><?php
                                if (!empty($val['tglpembayaran'])) {
                                    echo date("d/m/Y", strtotime($val['tglpembayaran']));
                                } else{
                                    echo '';
                                }
                                ?></td>
                              <td class="text-center"><?php echo $val['jenis_pembayaran'];?></td>
                              <td class="text-center"><b><?php
                                if(!empty($val['jumlahpembayaran'])){
                                $bayar = str_replace(',', '', $val['jumlahpembayaran']);
                                echo number_format($bayar);
                                } else{
                                    echo '-';
                                }
                                ?></b></td>
                              <td class="text-center"><?php echo $val['createdby'];?></td>
                              <td class="text-center"><?php echo date("d/m/Y H:i:s", strtotime($val['createddate']));?></td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td class="bg-dark text-center" colspan="2"><b> Saldo </b></td>
                              <td class="bg-dark text-center"><b><?php
                              if (isset($val['id_data'])) {
                              $ids = $val['id_data'];
                              $sqll = $this->db->query("select sum(jumlahpembayaran) as amounts from PAN_BRK.dbo.Histori_Rekon where id_data = '$ids'")->result_array();
                            }
                              if (!empty($sqll)) {
                              foreach ($sqll as $key => $values) {
                                  echo $saldo = number_format($values['amounts']);
                              }
                                } else{
                                    echo $saldo = '';
                                }
                                ?></b></td>
                                <td class="bg-dark text-center" colspan="2"></td>
                            </tr>
                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            </div>      
        </section>
      </div>

<script src="<?=base_url()?>assets/main/js/jquery-1.10.0.min.js"></script>
                    <script>
                        $(document).ready(function(){
                            var base_url = '<?php echo base_url();?>';
                        $('#cabang').change(function(){
                            var id = $('#cabang').val();
                            $.ajax({
                            type      : "POST",
                            url       : base_url + 'dokumenriau/get_capem',
                            data      : {id : id},
                            // async     : false,
                            dataType  : 'json',
                            success   : function(data) {
                                // alert(data);
                                var html = '';
                                var i;
                                for(i=0; i<data.length; i++){
                                    html += '<option value='+data[i].id_cabang+'>'+data[i].nama_cabang+'</option>';
                                }
                                $("#capem").html(html);
                        }
                        });
                        });
                    });
                </script>