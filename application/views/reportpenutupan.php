<?php
 
 header("Content-type: application/vnd-ms-excel");
 
 header("Content-Disposition: attachment; filename=$title.xls");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");
 
 ?>

 <table>
    <tr>
      <td style="font-size:20px;font-weight:bolder;" colspan="24">Rekap Penutupan Asuransi</td>
    </tr>
    <tr>
        <td colspan="24"></td>
    </tr>
    <tr>
      <td style="font-size:14px;font-weight:bolder;" colspan="24">Cabang : <?php
      $cab = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang
            where id_cabang = '$cabs'")->result_array();
        if (!empty($cab)) {
            foreach ($cab as $key => $cab1) {
                echo $cab1['nama_cabang'];
            }
        } else {
            echo '-';
            }
      ?></td>
    </tr>
    <tr>
      <td style="font-size:14px;font-weight:bolder;" colspan="24">Capem : <?php
      $cap = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang
            where id_cabang = '$caps'")->result_array();
        if (!empty($cap)) {
            if ($cap[0]['id_cabang'] != $cabs) {
                foreach ($cap as $key => $cap1) {
                    echo $cap1['nama_cabang'];
                }
            } else{
                echo '-';
            }
        } else {
            echo '-';
            }
      ?></td>
    </tr>
    <tr style="font-size:10px;font-weight:bolder;">
      <td colspan="24">Generate At <?=date("d-m-Y H:i:s")?></td>
    </tr>
    <tr>
        <td colspan="24"></td>
    </tr>
</table>
 
 <table border="1" width="100%">
 
      <thead>
           <tr style="background-color:#B0B0B0;font-size:14px;">
                <th class="text-center">No</th>
                <th class="text-center">Cabang</th>
                <th class="text-center">No PK</th>
                <th class="text-center">No Rekening</th>
                <th class="text-center">Nama</th>
                <th class="text-center">Tanggal Lahir</th>
                <th class="text-center">Tanggal Pembukaan</th>
                <th class="text-center">Jatuh Tempo</th>
                <th class="text-center">Plan Kredit</th>
                <th class="text-center"><div style="width: max-content;">Pekerjaan</div></th>
                <th class="text-center">Amount</th>
                <th class="text-center">No KTP</th>
                <th class="text-center">Rate</th>
                <th class="text-center">Jenis Kelamin</th>
                <th class="text-center">Rate Asuransi</th>
                <th class="text-center">Premi</th>
                <th class="text-center">Fee Based Income (FBI)</th>
                <th class="text-center">Jumlah Bayar</th>
                <th class="text-center">Selisih Bayar</th>
                <th class="text-center"><div style="width: max-content;">Tgl Bayar (Rekening Koran)</div></th>
                <th class="text-center">Asuransi</th>
                <th class="text-center">No NPWP</th>
                <th class="text-center">No PK Lama</th>
                <th class="text-center">Tanggal Produksi</th>
                <th class="text-center"><div style="width: max-content;">Tanggal Pengajuan Asuransi</div></th>
           </tr>
 
      </thead>
 
      <tbody>
        <?php 
        $no=1; foreach ($datana1 as $key => $value) { 
        ?>
            <tr id="<?php echo $value['id']; ?>">
            <td class="text-center"><?=$no++?></td>
            <td class="text-center">
                <div style="width: max-content;">
                <?php
            $a = $value['cab'];
            $cab = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang
            where id_cabang = '$a'")->result_array();
            foreach ($cab as $key => $cab1) {
                echo $cab1['nama_cabang'];
            }
            ?>
                </div>
            </td>
            <td class="text-center"><?=$value['pk']?></td>
            <td class="text-center"><?=$value['norek']?></td>
            <td class="text-center"><div style="width: max-content;"><?=$value['nama']?></div></td>
            <td class="text-center"><?=date("d/m/Y", strtotime($value['lahir']));?></td>
            <td class="text-center"><?=date("d/m/Y", strtotime($value['buka']));?></td>
            <td class="text-center"><?=$value['tempo']?></td>
            <td class="text-center">
                <div style="width: max-content;">
                <?php
                $b = $value['plankredit'];
                $plan = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterPlan
                where kode_plan = '$b'")->result_array();
                foreach ($plan as $key => $plan1) {
                    echo $plan1['nama_plan'];
                }
            ?>
            </div>
            </td>
            <td class="text-center">
                <div style="width: max-content;">
                <?php
                $id = $value['id'];
                $idkerja = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterDebitur
                where id_pekerjaan = '$id'")->result_array();
                foreach ($idkerja as $key => $idkerja1) {
                    echo $idkerja1['nama_pekerjaan'];
                }
            ?>
            </div>
            </td>
            <td class="text-center"><?=number_format($value['amount'], 0);?></td>
            <td class="text-center"><?=$value['ktp']?></td>
            <td class="text-center"><?=$value['rate'];?></td>
            <td class="text-center"><?php
                if ($value['sex'] == 'P'){
                    ?>
                <span class="badge badge-pill badge-success">
                    <?php
                    echo $value['sex'] = 'Perempuan';
                    } else if ($value['sex'] == 'L') {
                    ?>
                    <span class="badge badge-pill badge-danger">
                    <?php
                    echo $value['sex'] = 'Laki-Laki';
                    } else{
                        echo '';
                    }
                    ?></td>
                    <td class="text-center"><?=$value['rate_asuransi'];?></td>
                    <td class="text-center"><b><?php
                    if ($value['amount'] != '' && $value['rate_asuransi'] != '') {
                        $premi = ($value['amount'] * $value['rate_asuransi']) / 1000;
                    }
                        if (!empty($premi)) {
                            echo $premi = number_format(floatval($premi), 0);
                        } else{
                            echo $premi = '0';
                        }
                        
                    ?></b></td>
                    <td class="text-center"><?php
                        $fbi = str_replace(',', '', $premi) * 0.1;
                            echo $fbis = number_format($fbi);
                    ?></td>
                    <td class="text-center"><b><?php 
                    if($value['jumlah_bayar'] > 0){
                    $bayar = str_replace(',', '', $value['jumlah_bayar']);
                    echo number_format($bayar);
                    } else{
                        echo '-';
                    }
                    ?></b></td>
                    <td class="text-center"><?php 
                    $bayar = str_replace(',', '', $value['jumlah_bayar']);
                    if($bayar > 0){
                        $sel = str_replace(',', '', $premi) - $bayar;
                    echo number_format($sel);
                    } else{
                        echo '-';
                    }
                    ?></td>
                    <td class="text-center"><?php
                    if (!empty($value['tglbayarrk'])) {
                        echo date("d/m/Y", strtotime($value['tglbayarrk']));
                    } else{
                        echo '';
                    }
                    ?></td>
            <td class="text-center"><div style="width: max-content;">
                                    <?php
                                $ass = $value['asuransi'];
                                $asuransi = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterAsuransi
                                where id_asuransi = '$ass'")->result_array();
                                foreach ($asuransi as $key => $asu) {
                                    echo $asu['nama_asuransi'];
                                }
                                ?></div>
                                </td>
            <td class="text-center"><?=$value['npwp']?></td>
            <td class="text-center"><?=$value['old_pk']?></td>
            <td class="text-center"><div style="width: max-content;"><?=date("d/m/Y H:i:s", strtotime($value['date_created']));?></div></td>
            <td class="text-center"><?php
            if (!empty($value['tglpengajuanasuransi'])) {
                $tglasuransi = date("Ymd", strtotime($value['tglpengajuanasuransi']));
                if ($tglasuransi == 19700101) {
                    echo '-';
                } else{
                    echo date("d/m/Y", strtotime($value['tglpengajuanasuransi']));
                  }
            } else{
                echo '-';
            }
            ?></td>
        </tr>  
        <?php } ?>
      </tbody>
 
 </table>