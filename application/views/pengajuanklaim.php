 <div class="container-full">
		<!-- Main content -->
		<section class="content">
			<div class="row">
			<div class="col-12">
			  <div class="box box-default">
			  	<div class="box-header with-border">
                  <h4 class="box-title">Pengajuan Klaim</h4>
                </div>
				<!-- /.box-header -->
				<div class="box-body">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs customtab" role="tablist">
						<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home2" role="tab"><span class="hidden-sm-up"><i class="ion-home"></i></span> <span class="hidden-xs-down">Data</span></a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile2" role="tab"><span class="hidden-sm-up"><i class="ion-person"></i></span> <span class="hidden-xs-down">Tambah</span></a> </li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="home2" role="tabpanel">
						<div class="box">
				<div class="box-header with-border">
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Cabang :</label>
                                    <div class="input-group mb-3">
                                        <?php 
                                          $as = strtoupper($this->session->userdata('KodeUser'));
                                          $sub = substr($as,4);
                                        $user = $this->db->query("select * from PAN_BRK.dbo.DaftarUser
                                                 where KodeUser = '$as' ")->result_array();
                                        if (strpos($as, 'BRK') !== false) {
                                        $cabangs = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
                                        }
                                        if (strpos($as, 'BRK') === false) {
                                            $sql = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang where id_cabang = id_induk and nama_cabang LIKE '%cabang%'")->result_array();
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang">
                                            <option value=""></option>
                                         <?php
                                          foreach ($sql as $key => $value) {
                                         ?>
                                           <option value="<?=$value['id_cabang']?>"><?=$value['nama_cabang']?></option> 
                                         <?php
                                          } ?>
                                          </select>
                                          <?php
                                        }
                                        elseif(!empty($cabangs)){
                                            foreach ($cabangs as $key => $values) {
                                        ?>
                                        <select class="custom-select form-control" id="cabang" name="cabang">
                                            <option value=""></option>
                                           <option value="<?=$values['id_cabang']?>"><?=$values['nama_cabang']?></option> 
                                           </select>
                                        <?php
                                        }
                                         }else{
                                            $cab = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang where id_cabang = '$sub'")->result_array();
                                            $sub1 = $cab[0]['id_induk'];
                                            $sqlss = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang where id_cabang = '$sub1'")->result_array();
                                          foreach ($sqlss as $key => $vass) {
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang" disabled="">
                                            <option value="<?=$vass['id_cabang']?>"><?=$vass['nama_cabang']?></option> 
                                            </select>
                                         <?php
                                            }
                                        }
                                         ?>
                                </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Periode :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="location3" name="periodebulan">
                                        <option value=""></option>
                                        <option value="Amsterdam">Januari</option>
                                        <option value="Berlin">Februari</option>
                                        <option value="Frankfurt">Maret</option>
                                        <option value="Frankfurt">April</option>
                                        <option value="Frankfurt">Mei</option>
                                        <option value="Frankfurt">Juni</option>
                                        <option value="Frankfurt">Juli</option>
                                        <option value="Frankfurt">Agustus</option>
                                        <option value="Frankfurt">September</option>
                                        <option value="Frankfurt">Oktober</option>
                                        <option value="Frankfurt">November</option>
                                        <option value="Frankfurt">Desember</option>
                                    </select>
                                    <div>
                                        <select class="custom-select form-control" id="location3" name="periodetahun">
                                        <option value=""></option>
                                        <option value="Amsterdam">2021</option>
                                        <option value="Berlin">2022</option>
                                        <option value="Frankfurt">2023</option>
                                        <option value="Frankfurt">2024</option>
                                        <option value="Frankfurt">2025</option>
                                        <option value="Frankfurt">2026</option>
                                        <option value="Frankfurt">2027</option>
                                        <option value="Frankfurt">2028</option>
                                        <option value="Frankfurt">2029</option>
                                        <option value="Frankfurt">2030</option>
                                    </select>
                                    </div>
                                </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Capem / Kedai :</label>
                                    <div class="input-group mb-3">
                                        <?php 
                                          $as = strtoupper($this->session->userdata('KodeUser'));
                                        $sub = substr($as,4);
                                        $user = $this->db->query("select * from PAN_BRK.dbo.DaftarUser
                                                 where KodeUser = '$as' ")->result_array();
                                        if (strpos($as, 'BRK') !== false) {
                                        $cabangs = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
                                        }
                                        if (strpos($as, 'BRK') === false) {
                                         ?>
                                         <select class="custom-select form-control" id="capem" name="capem">
                                        </select>
                                         <?php
                                        } elseif(!empty($cabangs)){
                                        ?>
                                            <select class="custom-select form-control" id="capem" name="capem">
                                            </select>
                                        <?php
                                        } else{
                                            $sub = substr($as,4);
                                            $sqlss = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang where id_cabang = '$sub'")->result_array();
                                          foreach ($sqlss as $key => $va) {
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang" disabled="">
                                            <option value="<?=$va['id_cabang']?>"><?=$va['nama_cabang']?></option> 
                                            </select>
                                         <?php
                                            }
                                        }
                                         ?>
                                </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Masa Asuransi :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="capem" name="capem">
                                        <option value=""></option>
                                        <option value="">PAN BROKER</option>
                                        <option value="">PRA BROKER</option>
                                    </select>
                                </div>
                                </div>
                            </div>
                           </div>

                           <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Status Pembayaran :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="capem" name="capem">
                                        <option value=""></option>
                                        <option value="">Lunas</option>
                                        <option value="">Dalam Proses</option>
                                    </select>
                                </div>
                                </div>
                            </div>
                           </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="button" class="waves-effect waves-light btn btn-rounded btn-primary-light mb-5"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
                                </div>
                            </div>
                        </div>
                        <br>

					<div class="table-responsive">
					  <table id="example1" class="table">
						<thead class="bg-dark">
							<tr>
								<th>No. Klaim</th>
								<th>Tgl Pengajuan</th>
								<th>No. Surat Pengajuan</th>
								<th>No. Aplikasi</th>
								<th>Nama Nasabah</th>
								<th>Nilai Klaim yang Diajukan</th>
								<th>Nilai Klaim yang Dibayar</th>
								<th>Status Klaim</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
						<tfoot>
						</tfoot>
					  </table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
						</div>
						<div class="tab-pane" id="profile2" role="tabpanel">
						<div class="box box-default">
			<div class="box-header with-border">
			</div>
			<!-- /.box-header -->
			<div class="box-body wizard-content">
				<form action="#" class="tab-wizard wizard-circle">
					<!-- Step 1 -->
					<h6>Pengajuan Klaim</h6>
					<section>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">No. Klaim :</label>
									<input type="text" class="form-control" id="noklaim" name="noklaim" readonly=""></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">No. Surat Pengajuan Klaim dari Bank (Jika Pengajuan dari Telepon / Internet,ketikkan sesuai dengan kejadian pengajuan) :</label>
									<input type="text" class="form-control" id="nosuratpengajuan" name="nosuratpengajuan"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="firstName5">Tanggal Surat Pengajuan dari Bank :</label>
									<input type="date" class="form-control" id="date1" name="tglsuratbank"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">No. Aplikasi :</label>
									<div class="input-group mb-3">
									<input type="text" class="form-control" id="noaplikasi" name="noaplikasi">
									<div class="clearfix">
										<button type="button" class="waves-effect waves-light btn mb-8 bg-gradient-danger">Cari</button>
									</div>
								</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">Reffnumber :</label>
									<input type="text" class="form-control" id="reffnumber" name="reffnumber" readonly=""></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">No. Akad Kredit :</label>
									<input type="text" class="form-control" id="noakad" name="noakad" readonly=""></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">Nama Peserta Asuransi :</label>
									<input type="text" class="form-control" id="namapesertaasuransi" name="namapesertaasuransi" readonly=""></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">Tanggal Lahir :</label>
									<input type="text" class="form-control" id="tgllahir" name="tgllahir" readonly=""></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">No. Sertifikat Polis :</label>
									<input type="text" class="form-control" id="nosertifikatpolis" name="nosertifikatpolis" readonly=""></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">KC / KCP :</label>
									<input type="text" class="form-control" id="kckcp" name="kckcp" readonly=""></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">Nama Perusahaan Asuransi :</label>
									<input type="text" class="form-control" id="namaasuransi" name="namaasuransi" readonly=""></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">Produk :</label>
									<input type="text" class="form-control" id="produk" name="produk" readonly=""></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">Tipe Manfaat :</label>
									<input type="text" class="form-control" id="tipemanfaat" name="tipemanfaat" readonly=""></div>
							</div>
						</div>
						<div class="row">
						<div class="col-md-12">
								<div class="form-group">
									<label for="location3">Jenis Klaim :</label>
									<select class="custom-select form-control" id="location3" name="jenisklaim">
										<option value=""></option>
										<option value="Amsterdam">Meninggal Dunia</option>
										<option value="Berlin">Kredit Macet</option>
										<option value="Frankfurt">Pemutusan Hubungan Kerja (PHK)</option>
									</select>
								</div>
							</div>
						</div>
							<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="firstName5">Tanggal Kejadian :</label>
									<input type="date" class="form-control" id="date1" name="tglkejadian"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">Nilai Plafond Kredit (yang dipertanggungkan) :</label>
									<input type="text" class="form-control" id="nilaiplafon" name="nilaiplafon" readonly="" placeholder="0"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">Nilai Pengajuan Klaim :</label>
									<input type="text" class="form-control" id="nilaipengajuanklaim" name="nilaipengajuanklaim" placeholder="0"></div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<button type="button" class="waves-effect waves-light btn btn-info mb-7">Save</button>
								</div>
							</div>
						</div>


					</section>
					<!-- Step 2 -->
					<h6>Info Ke Asuransi</h6>
					<section>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">No. Surat :</label>
									<input type="text" class="form-control" id="nosurat" name="nosurat"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">Perihal :</label>
									<input type="text" class="form-control" id="nosurat" name="perihal" value="Pemberitahuan Klaim kepada Perusahaan Asuransi"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="firstName5">Tanggal Surat Pemberitahuan ke Asuransi :</label>
									<input type="date" class="form-control" id="date1" name="tglpemberitahuanasuransi"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Upload Dokumen</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="uploaddokumen">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<button type="button" class="waves-effect waves-light btn btn-info mb-7">Save</button>
								</div>
							</div>
						</div>
					</section>
					<!-- Step 3 -->
					<h6>Kelengkapan & Persetujuan Klaim</h6>
					<section>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Formulir Klaim</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="formulirklaim">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>

					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Kartu Keluarga (KK) Ahli Waris</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="kkahliwaris">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>

					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Kartu Tanda Penduduk (KTP) Ahli Waris</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="ktpahliwaris">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>

					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Copy Kwitansi Pembayaran Premi Terakhir / Copy Bukti Transfer Pembayaran Premi</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="copykwitansi">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>

					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Berita Acara Klaim</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="bak">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>

					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Berita Acara dari Pihak Kepolisian setempat jika kecelakaan dan SIM Nasabah jika sebagai Pengemudi</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="bakkepolisian">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>

					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Surat Kematian dari Rumah Sakit dan Formulir Hasil Diagnosa Dokter jika meninggal dunia di Rumah Sakit</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="suratkematianrs">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Surat Keterangan Kematian dari Kelurahan dan Surat Kronologis meninggal dunia dari Ahli Waris diatas Materai jika meninggal di Rumah</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="suratkematiankelurahan">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Surat Keterangan Kematian dari KBRI jika meninggal dunia di Luar Negeri</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="suratkematianluarnegeri">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Surat Putusan Pengadilan yang menyatakan Nasabah hilang dalam suatu musibah</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="suratputusanpengadilan">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Copy / Asli Polis</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="poliss">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>

					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Copy Akad Pembiayaan antara Bank dengan Nasabah</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="copyakad">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Kartu Tanda Penduduk (KTP)</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="ktp">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Kartu Keluarga (KK)</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="kk">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>

					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Posisi terakhir Pembiayaan sebelum terjadinya Klaim termasuk Pembiayaan Atribusi (Print Screen Baki Debet)</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="pembiayaanterakhirklaim">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Daftar Jadwal Angsuran dan Realisasi Pembayaran Angsuran s/d Posisi Terakhir saat terjadi Klaim</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="daftarangsurans">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Surat Kuasa kepada Bank untuk memotong gaji jika TC Polis mensyaratkan Pembayaran Angsuran melalui Payroll</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="suratkuasabank">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Surat Kuasa kepada Bank untuk memotong gaji jika TC Polis mensyaratkan Pembayaran Angsuran melalui Payroll</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="suratkuasabank">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Surat Keterangan Ahli Waris yang menyatakan Ahli Waris yang dilegalisasi oleh Kelurahan</label>
									<div class="custom-file">
								<input type="file" class="custom-file-input" id="customFile" name="skahliwaris">
								<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">Nilai Klaim Diajukan :</label>
									<input type="text" class="form-control" id="nilaipengajuanklaim1" name="nilaipengajuanklaim1" readonly="" placeholder="0"></div>
							</div>
						</div>
						<div class="row">
						<div class="col-md-12">
								<div class="form-group">
									<label for="location3">Claimable :</label>
									<select class="custom-select form-control" id="location3" name="jenisklaim">
										<option value=""></option>
										<option value="Amsterdam">Claimable</option>
										<option value="Berlin">Unclaimable</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">Nilai Klaim Disetujui :</label>
									<input type="text" class="form-control" id="nilaiklaimdisetujui" name="nilaiklaimdisetujui" placeholder="0"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="firstName5">Tanggal Nilai Klaim Disetujui :</label>
									<input type="date" class="form-control" id="date1" name="tglnilaiklaimdisetujui"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="firstName5">Tanggal Rencana Pembayaran Klaim :</label>
									<input type="date" class="form-control" id="date1" name="tglrencanapembayaran"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<button type="button" class="waves-effect waves-light btn btn-info mb-7">Save</button>
								</div>
							</div>
						</div>
					</section>
					<!-- Step 4 -->
					<h6>Penitipan Klaim Ke Broker</h6>
					<section>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">Nilai Pengajuan Klaim :</label>
									<input type="text" class="form-control" id="nilaipengajuanklaim2" name="nilaipengajuanklaim2" readonly="" placeholder="0"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">Nilai Persetujuan Klaim :</label>
									<input type="text" class="form-control" id="nilaipersetujuanklaim" name="nilaipersetujuanklaim" readonly="" placeholder="0"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="firstName5">Nilai Klaim yang Dibayar :</label>
									<div class="input-group mb-3">
								<input type="text" class="form-control" id="nilaiklaimdibayar" name="nilaiklaimdibayar" placeholder="0">
									<div class="clearfix">
										<button type="button" class="waves-effect waves-light btn mb-8 bg-gradient-danger">SAMAKAN</button>
									</div>
								</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="firstName5">Tanggal Pembayaran Klaim :</label>
									<input type="date" class="form-control" id="date1" name="tglpembayaran"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<button type="button" class="waves-effect waves-light btn btn-info mb-7">Save</button>
								</div>
							</div>
						</div>
					</section>

					<h6>Pembayaran Klaim</h6>
					<section>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">Nilai Pengajuan Klaim :</label>
									<input type="text" class="form-control" id="nilaipengajuanklaim3" name="nilaipengajuanklaim3" readonly="" placeholder="0"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">Nilai Persetujuan Klaim :</label>
									<input type="text" class="form-control" id="nilaipersetujuanklaim1" name="nilaipersetujuanklaim1" readonly="" placeholder="0"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="firstName5">Nilai Klaim yang dibayar ke Broker :</label>
									<input type="text" class="form-control" id="nilaiklaimdibayarbroker" name="nilaiklaimdibayarbroker" readonly="" placeholder="0"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="firstName5">Nilai Klaim yang Dibayar ke Bank :</label>
									<div class="input-group mb-3">
								<input type="text" class="form-control" id="nilaiklaimdibayarbank" name="nilaiklaimdibayarbank" placeholder="0">
									<div class="clearfix">
										<button type="button" class="waves-effect waves-light btn mb-8 bg-gradient-danger">SAMAKAN</button>
									</div>
								</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="firstName5">Tanggal Pembayaran Klaim :</label>
									<input type="date" class="form-control" id="date1" name="tglpembayaran1"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<button type="button" class="waves-effect waves-light btn btn-info mb-7">Save</button>
								</div>
							</div>
						</div>
					</section>
				</form>
			</div>
			<!-- /.box-body -->
		  </div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			</div>		
		</section>
		<!-- /.content -->
	  </div>