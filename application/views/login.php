<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from eduadmin-template.multipurposethemes.com/bs4/main/auth_login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Sep 2021 02:26:41 GMT -->
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?=base_url()?>assets/images/pan.ico">

    <title>Log in </title>
  
	<!-- Vendors Style-->
	<link rel="stylesheet" href="<?=base_url()?>assets/main/css/vendors_css.css">
	  
	<!-- Style-->  
	<link rel="stylesheet" href="<?=base_url()?>assets/main/css/style.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/main/css/skin_color.css">

</head>
	
<body class="hold-transition theme-primary bg-img" style="background-image: url(<?=base_url()?>assets/images/auth-bg/bg-1.jpg)">
	
	<div class="container h-p100">
		<div class="row align-items-center justify-content-md-center h-p100">	
			
			<div class="col-12">
				<div class="row justify-content-center no-gutters">
					<div class="col-lg-5 col-md-5 col-12">
						<div class="bg-white rounded30 shadow-lg">
							<div class="content-top-agile p-20 pb-0">
								<img alt="image" src="<?php echo base_url(); ?>assets/images/pan.png" style="display: block;margin-left: auto;margin-right: auto;">
								<br>
								<h5 class="mb-0">PT. Proteksi Antar Nusa<h5>							
							</div>
							<div class="p-40">
								<?= $this->session->flashdata('message');?>
								<form action="<?php echo base_url('login');?>" method="post">
									<div class="form-group">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
											</div>
											<input type="text" name="KodeUser" class="form-control pl-15 bg-transparent" placeholder="Username" required="" autocomplete="off">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text  bg-transparent"><i class="ti-lock"></i></span>
											</div>
											<input type="password" name="Password" class="form-control pl-15 bg-transparent" placeholder="Password" required="" autocomplete="off">
										</div>
									</div>
									  <div class="row">
										<div class="col-6">
										</div>
										<!-- /.col -->
										<div class="col-12 text-center">
										  <button type="submit" class="btn btn-danger mt-10">SIGN IN</button>
										  <br>
										<br>
										</div>

										<div class="col-12 text-center">
										  <button type="button" class="col-12 waves-effect waves-light btn mb-5 bg-gradient-danger">Simulasi Biaya Nasabah</button>
										</div>
										<!-- /.col -->
									  </div>
								</form>	
							</div>						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Vendor JS -->
	<script src="<?=base_url()?>assets/main/js/vendors.min.js"></script>
	<script src="<?=base_url()?>assets/main/js/pages/chat-popup.js"></script>
    <script src="<?=base_url()?>assets/assets/assets/icons/feather-icons/feather.min.js"></script>	

</body>

<!-- Mirrored from eduadmin-template.multipurposethemes.com/bs4/main/auth_login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Sep 2021 02:26:45 GMT -->
</html>
