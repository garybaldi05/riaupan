 <div class="container-full">
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-12">
			  <div class="box box-default">
				<div class="box-header with-border">
				  <h4 class="box-title">Daftar Pembayaran Restitusi</h4>
				</div>
				<!-- /.box-header -->

                <div class="box-body">
                    <form class="form-horizontal form-bordered" method="post" action="<?=base_url('pembayaranrestitusi/search')?>">
                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Cabang :</label>
                                    <div class="input-group mb-3">
                                        <?php 
                                          $as = strtoupper($this->session->userdata('KodeUser'));
                                          $sub = substr($as,4);
                                        $user = $this->db->query("select * from PAN_BRK.dbo.DaftarUser
                                                 where KodeUser = '$as' ")->result_array();
                                        if (strpos($as, 'BRK') !== false) {
                                        $cabangs = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
                                        }
                                        if (strpos($as, 'BRK') === false) {
                                            $sql = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang where id_cabang = id_induk and nama_cabang LIKE '%cabang%'")->result_array();
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang">
                                            <option value=""></option>
                                         <?php
                                          foreach ($sql as $key => $value) {
                                         ?>
                                           <option value="<?=$value['id_cabang']?>"><?=$value['nama_cabang']?></option> 
                                         <?php
                                          } ?>
                                          </select>
                                          <?php
                                        }
                                        elseif(!empty($cabangs)){
                                            foreach ($cabangs as $key => $values) {
                                        ?>
                                        <select class="custom-select form-control" id="cabang" name="cabang">
                                            <option value=""></option>
                                           <option value="<?=$values['id_cabang']?>"><?=$values['nama_cabang']?></option> 
                                           </select>
                                        <?php
                                        }
                                         }else{
                                            $cab = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang where id_cabang = '$sub'")->result_array();
                                            $sub1 = $cab[0]['id_induk'];
                                            $sqlss = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang where id_cabang = '$sub1'")->result_array();
                                          foreach ($sqlss as $key => $vass) {
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang" disabled="">
                                            <option value="<?=$vass['id_cabang']?>"><?=$vass['nama_cabang']?></option> 
                                            </select>
                                         <?php
                                            }
                                        }
                                         ?>
                                </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName5">Periode :</label>
                                    <div class="input-group mb-3">
                                    <select class="custom-select form-control" id="location3" name="periodebulan">
                                        <option value=""></option>
                                        <option value="01">Januari</option>
                                        <option value="02">Februari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                    <div>
                                        <select class="custom-select form-control" id="location3" name="periodetahun">
                                        <option value=""></option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                        <option value="2026">2026</option>
                                        <option value="2027">2027</option>
                                        <option value="2028">2028</option>
                                        <option value="2029">2029</option>
                                        <option value="2030">2030</option>
                                    </select>
                                    </div>
                                </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Capem / Kedai :</label>
                                    <div class="input-group mb-3">
                                        <?php 
                                          $as = strtoupper($this->session->userdata('KodeUser'));
                                        $sub = substr($as,4);
                                        $user = $this->db->query("select * from PAN_BRK.dbo.DaftarUser
                                                 where KodeUser = '$as' ")->result_array();
                                        if (strpos($as, 'BRK') !== false) {
                                        $cabangs = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang where id_cabang = id_induk and id_cabang = '$sub'")->result_array();
                                        }
                                        if (strpos($as, 'BRK') === false) {
                                         ?>
                                         <select class="custom-select form-control" id="capem" name="capem">
                                        </select>
                                         <?php
                                        } elseif(!empty($cabangs)){
                                        ?>
                                            <select class="custom-select form-control" id="capem" name="capem">
                                            </select>
                                        <?php
                                        } else{
                                            $sub = substr($as,4);
                                            $sqlss = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang where id_cabang = '$sub'")->result_array();
                                          foreach ($sqlss as $key => $va) {
                                         ?>
                                         <select class="custom-select form-control" id="cabang" name="cabang" disabled="">
                                            <option value="<?=$va['id_cabang']?>"><?=$va['nama_cabang']?></option> 
                                            </select>
                                         <?php
                                            }
                                        }
                                         ?>
                                </div>
                                </div>
                            </div>
                            
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="waves-effect waves-light btn btn-rounded btn-primary-light mb-5"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
                                </div>
                            </div>
                        </div>
                        </form>
                  </div>
			  </div>
			  <!-- /.box -->
			</div>
			</div>		
		</section>
		<!-- /.content -->

                <section class="content">
            <div class="row">
                <div class="col-12">
              <div class="box box-default">
                <div class="box-body">
                    <div class="table-responsive">
                      <table id="example1" class="table">
                        <thead class="bg-dark">
                            <tr>
                                <th class="text-center">No.</th>
                                <th class="text-center">Action</th>
                                <th class="text-center">No. Restitusi</th>
                                <th class="text-center">No. Surat</th>
                                <th class="text-center">Cabang</th>
                                <th class="text-center">Tgl. Pengajuan</th>
                                <th class="text-center">No PK</th>
                                <th class="text-center">Reffnumber</th>
                                <th class="text-center">No. Akad Kredit</th>
                                <th class="text-center">Nama</th>
                                <th class="text-center">Tgl. Lahir</th>
                                <th class="text-center">No. Polis</th>
                                <th class="text-center">Asuransi</th>
                                <th class="text-center">Produk</th>
                                <th class="text-center">Tipe Manfaat</th>
                                <th class="text-center">Nilai Plafon</th>
                                <th class="text-center">Tenor (dalam bulan)</th>
                                <th class="text-center">Tgl. Mulai Akad</th>
                                <th class="text-center">Tgl. Selesai Akad</th>
                                <th class="text-center">Premi Dibayarkan</th>
                                <th class="text-center">Tgl. Pelunasan</th>
                                <th class="text-center">Sisa Jangka Waktu</th>
                                <th class="text-center">Jangka Waktu yang sudah dijalani</th>
                                <th class="text-center">Status Restitusi</th>
                                <th class="text-center">Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $no=1; foreach ($datana as $key => $value) { ?>
                                <tr id="<?php echo $value['no_restitusi']; ?>">
                                <td class="text-center"><?=$no++?></td>
                                <td class="text-center">
                                    <div style="width: max-content;">
                                    <a title="View"
                                        href="javascript:;"
                                        data-No_Journal="<?php echo $value['no_restitusi'];?>"
                                        data-toggle="modal" 
                                        data-target="#view<?php echo $value['no_restitusi']?>"
                                        class="btn-sm btn-warning"
                                        >
                                        <i class="glyphicon glyphicon-eye-open"></i>
                                    </a> &nbsp;
                                    <a title="Change"
                                       href="javascript:;"
                                       data-No_Journal="<?php echo $value['no_restitusi'];?>"
                                       data-toggle="modal"
                                       data-target="#edit<?php echo $value['no_restitusi']?>"
                                       class="btn-sm btn-info"
                                    >
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </a> &nbsp;
                                    <a title="Delete" href="<?php echo base_url('pengajuanrestitusi/delete/'.$value['no_restitusi'])?>" onclick="return confirm('Are your sure ?');" class="btn-sm btn-danger remove">
                                        <i class="glyphicon glyphicon-remove-sign"></i>
                                    </a>
                                    </div>
                                </td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['no_restitusi'];?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['no_surat'];?></div></td>
                                <td class="text-center">
                                    <div style="width: max-content;">
                                    <?php
                                $a = $value['cab'];
                                $cab = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterCabang
                                where id_cabang = '$a'")->result_array();
                                foreach ($cab as $key => $cab1) {
                                    echo $cab1['nama_cabang'];
                                }
                                ?>
                                    </div>
                                </td>
                                <td class="text-center"><div style="width: max-content;"><?=date("d/m/Y", strtotime($value['tgl_pengajuan']));?></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['no_pk']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['reffnumber']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['no_akad']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['nama']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=date("d/m/Y", strtotime($value['tgl_lahir']));?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['no_polis']?></div></td>
                                <td class="text-center"><div style="width: max-content;">
                                    <?php
                                $ass = $value['asuransi'];
                                $asuransi = $this->db->query("SELECT * FROM PAN_BRK.dbo.MasterAsuransi
                                where id_asuransi = '$ass'")->result_array();
                                foreach ($asuransi as $key => $asu) {
                                    echo $asu['nama_asuransi'];
                                }
                                ?></div>
                                </td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['produk'];?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['tipe_manfaat'];?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=number_format($value['nilai_plafon'], 0);?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['tenor']?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=date("d/m/Y", strtotime($value['tgl_akadmulai']));?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=date("d/m/Y", strtotime($value['tgl_akadselesai']));?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=number_format($value['premi_bayar'], 0);?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=date("d/m/Y", strtotime($value['tgl_pelunasan']));?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['jk_sisa'];?>?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?=$value['jk_berjalan'];?>?></div></td>
                                <td class="text-center"><div style="width: max-content;"><?php
                                if ($value['status'] == '0'){
                                        ?>
                                    <span class="badge badge-pill badge-warning">
                                        <?php
                                        echo $value['status'] = 'No';
                                        } else if ($value['status'] == '1') {
                                        ?>
                                        <span class="badge badge-pill badge-danger">
                                        <?php
                                        echo $value['status'] = 'Ditolak';
                                        } else if($value['status'] == '2'){
                                        ?>
                                        <span class="badge badge-pill badge-success">
                                        <?php
                                            echo $value['status'] = 'Yes';
                                        }
                                        ?>
                                    </td>
                                    <td class="text-center"><?=$value['keterangan']?></td></td>
                            </tr>  
                            <?php } ?>
                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            </div>      
        </section>

	  </div>
      <script src="<?=base_url()?>assets/main/js/jquery-1.10.0.min.js"></script>
                    <script>
                        $(document).ready(function(){
                            var base_url = '<?php echo base_url();?>';
                        $('#cabang').change(function(){
                            var id = $('#cabang').val();
                            $.ajax({
                            type      : "POST",
                            url       : base_url + 'pembayaranrestitusi/get_capem',
                            data      : {id : id},
                            // async     : false,
                            dataType  : 'json',
                            success   : function(data) {
                                // alert(data);
                                var html = '';
                                var i;
                                for(i=0; i<data.length; i++){
                                    html += '<option value='+data[i].id_cabang+'>'+data[i].nama_cabang+'</option>';
                                }
                                $("#capem").html(html);
                        }
                        });
                        });
                    });
                </script>